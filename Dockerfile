#FROM browserless/chrome as build
FROM node:10 as build

# Install Google Chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update && apt-get install -y google-chrome-stable

WORKDIR /app
ENV PATH "$PATH:/app/node_modules/.bin"
ENV CHROME_BIN=google-chrome-stable

COPY . .
RUN npm install
RUN npm install -g ionic
RUN npm install -g @ionic/angular
RUN npm rebuild node-sass
RUN npm install -g sonarqube-scanner
RUN npm install -g cordova
RUN npm run lint
RUN ionic build
RUN npm run test
RUN npm run sonar


# Create nginix install
FROM nginx:stable
COPY --from=build /app/www /var/www/

# Configure and launch
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 3000
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
