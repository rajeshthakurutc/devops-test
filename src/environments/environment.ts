// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment = {
  production: false,
  appVersion: require('../../package.json').version,
  reportingLink: 'https://azure-paas-dx.azurewebsites.net/reporting',
  target: 'dev',
  API_HOST: 'https://azure-paas-dx1.azure-api.net/',
  API_SUBSCRIPTION_KEY: 'f7675d9731e4434a9846f6b2a57ba88a',
  INSTRUMENTATION_KEY: '4689ced2-c56a-44bb-887e-3abb58f938c3'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
