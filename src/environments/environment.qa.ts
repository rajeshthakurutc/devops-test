// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment = {
  production: false,
  appVersion: require('../../package.json').version,
  target: 'qa',
  reportingLink: 'https://azure-paas-qa.azurewebsites.net/reporting',
  API_HOST: 'https://azure-paas-qa.azure-api.net/',
  API_SUBSCRIPTION_KEY: 'dd8c2ee880ae45f3901172c1d411ea5f',
  INSTRUMENTATION_KEY: '24570fbf-8782-4c49-bdae-e0fc339a2f08'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
