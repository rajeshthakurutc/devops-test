import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from 'app/shared/auth.service';
import { endpoints } from 'app/configs';

@Component({
  selector: 'stm-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss']
})
export class ChangePasswordPageComponent implements OnInit {
  currPass = '';
  newPass = '';
  confirmPass = '';
  lowercase = 'hydrated';
  uppercase = 'hydrated';
  number = 'hydrated';
  characterMin = 'hydrated';

  newPasswordErrors = [
    {
      req: 'length',
      text: 'Between {x} and {y} characters',
      length: -1
    },
    {
      req: 'lowercase',
      text: 'At least {x} lowercase character(s)',
      length: -1
    },
    {
      req: 'uppercase',
      text: 'At least {x} uppercase character(s)',
      length: -1
    },
    {
      req: 'numeric',
      text: 'At least {x} digits(s)',
      length: -1
    },
    {
      req: 'special',
      text: 'At least x special character(s) ($, !, &, etc.)',
      length: -1
    }
  ];

  reqsVisible = true;
  oldPasswordWrongError = true;
  newPasswordMismatchError = true;

  // Try a bitwise operator
  // 0000;
  // 1 first
  // 3 first and second
  // 7 first through third
  // 15 all

  // 2 second
  // 4 third
  // 8 fourth

  // 5 first and third
  // 6 second and third
  // 9 first and third
  // 10 second and fourth

  // 11 1st, 2nd, 4th
  // 12 3rd and 4th
  // 13 1st, 3rd and 4th
  // 14 2nd, 3rd, 4th

  constructor(private router: Router, private location: Location, private authService: AuthService) {}

  ngOnInit(): void {
    const organizationId = this.authService.getUserInfo().organizationId;
    endpoints.getSecurityPolicy = endpoints.getSecurityPolicy.replace('{organizationId}', organizationId);
    this.authService.fetchSecurityPolicy().subscribe(data => {
      console.log(data);
    });
  }

  updatePassword(): void {
    console.log('update password');
  }

  passwordCheck(): void {
    this.reqsVisible = true;
    if (this.newPass.search(/[a-z]/g) !== -1) {
      // Change the checkmark
      this.lowercase = 'hydrated green';
    } else {
      this.lowercase = 'hydrated';
    }
    if (this.newPass.search(/[A-Z]/g) !== -1) {
      this.uppercase = 'hydrated green';
    } else {
      this.uppercase = 'hydrated';
    }
    if (this.newPass.search(/[0-9]/g) !== -1) {
      this.number = 'hydrated green';
    } else {
      this.number = 'hydrated';
    }
    if (this.newPass.length >= 7) {
      this.characterMin = 'hydrated green';
    } else {
      this.characterMin = 'hydrated';
    }
  }

  checkLength(str, min?, max?) {
    min = min || 0;
    max = max || Infinity;
    return str.length >= min && str.length >= max;
  }

  checkLower(str) {
    return /[a-z]/g.test(str);
  }

  checkUpper(str) {
    return /[A-Z]/g.test(str);
  }

  checkNumeric(str) {
    return /[0-9]/g.test(str);
  }

  checkSpecial(str) {
    return /[^a-zA-Z\d\s:]/g.test(str);
  }
}
