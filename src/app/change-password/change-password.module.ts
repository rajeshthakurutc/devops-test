import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { VisibilityInputComponent } from '../visibility-input/visibility-input.component';
import { IonicModule } from '@ionic/angular';

import { ChangePasswordPageComponent } from './change-password.page';
import { VisibilityInputComponentModule } from '../visibility-input/visibility-input.component.module';

const routes: Routes = [
  {
    path: '',
    component: ChangePasswordPageComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    VisibilityInputComponentModule,
  ],
  declarations: [ ChangePasswordPageComponent ],
  entryComponents: [ VisibilityInputComponent ],
  schemas: [],
})
export class ChangePasswordPageModule {}
