import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { IonicModule } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Camera } from '@ionic-native/camera/ngx';

import { MomentModule } from 'ngx-moment';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { KitConfirmationComponent } from './installation-wizard/kit-confirmation/kit-confirmation.component';
import { ScanGatewayComponent } from './installation-wizard/scan-gateway/scan-gateway.component';
import { MonitorSetupComponent } from './installation-wizard/monitor-setup/monitor-setup.component';
import { GatewaySetupComponent } from './installation-wizard/gateway-setup/gateway-setup.component';
import { ZoneInstructionsComponent } from './installation-wizard/zone-instructions/zone-instructions.component';
import { ZoneSummaryComponent } from './installation-wizard/zone-summary/zone-summary.component';
import { AddDeviceComponent } from './installation-wizard/add-device/add-device.component';
import { VisibilityInputComponentModule } from './visibility-input/visibility-input.component.module';
import { Globals } from 'app/globals';
import { CorrectiveActionComponent } from './tasks/corrective-action/corrective-action.component';
import { PlaceMonitorComponent } from './installation-wizard/place-monitor/place-monitor.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NavbarService } from './navbar/navbar.service';
import { HistoryblockComponent } from './historyblock/historyblock.component';
import { GraphComponent } from './graph/graph.component';
import { TaskDetailComponent } from './tasks/task-detail/task-detail.component';
import { SuccessScreenComponent } from './tasks/success-screen/success-screen.component';
import { FAQComponent } from './help/faq/faq.component';
import { ContactUsComponent } from './help/contact-us/contact-us.component';
import { MyProfileComponent } from './settings/my-profile/my-profile.component';
import { NotificationManagementComponent } from './settings/notification-management/notification-management.component';
import { SystemLogsComponent } from './settings/system-logs/system-logs.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LicenseComponent } from './terms/license/license.component';
import { PrivacyComponent } from './terms/privacy/privacy.component';
import { GlobalHTTPInterceptor } from './http-interceptor';
import { ZonesModule } from './zones/zones.module';
import { LoaderInterceptor } from './loader-interceptor';
import { LoaderComponent } from './loader/loader.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    KitConfirmationComponent,
    ScanGatewayComponent,
    MonitorSetupComponent,
    GatewaySetupComponent,
    ZoneInstructionsComponent,
    ZoneSummaryComponent,
    AddDeviceComponent,
    PlaceMonitorComponent,
    NavbarComponent,
    CorrectiveActionComponent,
    HistoryblockComponent,
    GraphComponent,
    TaskDetailComponent,
    SuccessScreenComponent,
    FAQComponent,
    ContactUsComponent,
    MyProfileComponent,
    NotificationManagementComponent,
    SystemLogsComponent,
    WelcomeComponent,
    LicenseComponent,
    PrivacyComponent,
    LoaderComponent,
    AboutComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot({ mode: 'md' }),
    AppRoutingModule,
    MomentModule,
    FormsModule,
    HttpClientModule,
    VisibilityInputComponentModule,
    ZonesModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Globals,
    BarcodeScanner,
    Camera,
    NavbarService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GlobalHTTPInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
