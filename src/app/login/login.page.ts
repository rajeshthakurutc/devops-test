import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';

import { LoginService } from './login.service';
import { AuthService } from '../shared/auth.service';
import { AnalyticsService } from '../shared/analytics.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'stm-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPageComponent implements OnInit {
  email: string;
  password: string;
  emailSent = false;
  authenticationError = false;
  errorMsg: string;
  loading = false;
  appVersion: string = environment.appVersion;
  isProd: boolean = environment.production;
  envTarget: string = environment.target;

  constructor(
    private loginService: LoginService,
    private authService: AuthService,
    public analytics: AnalyticsService,
    public alertController: AlertController,
    public loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.submit = this.submit.bind(this);
    this.forgotPass = this.forgotPass.bind(this);
  }

  submit() {
    this.authenticationError = false;
    this.loading = true;
    this.loginService.fetchAuthToken(this.email, this.password).subscribe(
      tokens => {
        const { access_token }: any = tokens;
        this.authService.setTokens(tokens);
        this.authService.updateHeadersWithAuthToken(access_token);
        this.authenticationError = false;
        this.loading = false;

        this.loginService.fetchUserInfo().subscribe(user => {
          this.authService.setUserInfo(user);
          this.authService.setHashedUserForAppInsights(this.authService.userInfo.userName);
          this.email = '';
          this.password = '';
        });
        this.loginService.fetchAgreements().subscribe(agreements => {
          this.loginService.updateAgreementsAndRedirectUser(agreements);
        });
      },
      err => {
        const errorType = err.error.error;
        this.authenticationError = true;
        this.loading = false;

        if (errorType === 'invalid_username_or_password') {
          this.errorMsg = 'Incorrect username or password.';
        } else if (errorType === 'account_locked') {
          this.errorMsg =
            'Your account has been locked. Call <a href="tel:1-800-843-8367">1-800-843-8367</a> to resolve.';
        } else {
          // tslint:disable-next-line: max-line-length
          this.errorMsg = `There is an issue with your account. Call <a href="tel:1-800-843-8367">1-800-843-8367</a> to resolve.`;
        }
      }
    );
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      translucent: true,
      cssClass: 'loading-override'
    });
    return await loading.present();
  }

  forgotPass() {
    this.openAlert('resend');
  }

  resetPassword(username): any {
    this.presentLoadingWithOptions();
    this.loginService.resetPassword(username).subscribe(
      () => {
        this.loadingController.dismiss();
        return this.done();
      },
      () => {
        this.loadingController.dismiss();
        this.resetPasswordError();
      }
    );
  }

  done() {
    this.openAlert('done');
  }

  resetPasswordError() {
    this.openAlert('error');
  }

  async openAlert(alertType) {
    const content = {
      resend: {
        header: 'Enter your username to reset password',
        inputs: [
          {
            name: 'email',
            type: 'text',
            label: 'email',
            placeholder: 'Username'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          },
          {
            text: 'Reset',
            handler: data => {
              this.resetPassword(data.email);
            }
          }
        ]
      },
      done: {
        header: 'A password reset link will be sent to you if the provided username exists.',
        cssClass: 'alert-body-copy',
        buttons: [
          {
            text: 'Done',
            handler: data => {
              console.log('Close modal', data);
            }
          }
        ]
      },
      error: {
        header: 'Your request cannot be processed. Please contact your System Administrator for assistance.',
        cssClass: 'alert-body-copy',
        buttons: [
          {
            text: 'Done'
          }
        ]
      }
    };

    const alert = await this.alertController.create(content[alertType]);

    await alert.present();
  }
}
