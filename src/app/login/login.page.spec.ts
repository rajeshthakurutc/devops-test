import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Router, RouterModule } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';

import { UtilsService } from '../shared/utils.service';
import { AuthService } from '../shared/auth.service';
import { LoginPageComponent } from './login.page';
import { VisibilityInputComponent } from '../visibility-input/visibility-input.component';

describe('LoginPage', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;
  let alertControllerSpy, utilsServiceSpy, authServiceSpy;

  beforeEach(async(() => {
    alertControllerSpy = jasmine.createSpyObj('AlertController', ['create']);
    utilsServiceSpy = jasmine.createSpyObj('UtilsService', ['log', 'handleError']);
    authServiceSpy = jasmine.createSpyObj('AuthService', ['setTokens', 'updateHeadersWithAuthToken']);

    TestBed.configureTestingModule({
      declarations: [LoginPageComponent, VisibilityInputComponent],
      imports: [FormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: AlertController, useValue: alertControllerSpy },
        { provide: AuthService, useValue: authServiceSpy },
        { provide: UtilsService, useValue: utilsServiceSpy },
        { provide: Router, userClass: RouterModule },
        HttpClient,
        HttpHandler,
        LoadingController
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
