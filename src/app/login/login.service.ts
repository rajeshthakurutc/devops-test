import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
/* tslint:disable-next-line:import-blacklist */
import { Observable } from 'rxjs';

import { UtilsService } from '../shared/utils.service';
import { authHttpOptions, httpOptions, endpoints } from '../configs';

@Injectable({ providedIn: 'root' })
export class LoginService {
  isEulaAccepted: boolean;
  isPrivacyAccepted: boolean;
  isSetupCompleted = false;

  constructor(private http: HttpClient, private utilsService: UtilsService, private router: Router) {}

  fetchAuthToken(username, password): Observable<{}> {
    const formData = `grant_type=password&scope=rtweb.api offline_access&username=${username}&password=${password}`;
    return (
      this.http
        .post(endpoints.authenticateUser, formData, authHttpOptions)
        // don't catch error here, handle in the component
        .pipe(tap(_ => this.utilsService.log('fetched auth token')))
    );
  }

  fetchAgreements(): Observable<{}> {
    return this.http.get(endpoints.getAgreements, httpOptions).pipe(
      tap(_ => this.utilsService.log('fetched agreements')),
      catchError(this.utilsService.handleError('fetchAgreements', []))
    );
  }

  updateAgreementStatus(agreements: object): void {
    // requiresEula and requiresPrivacyPolicy will be true if the user has NOT agreed to them
    this.isEulaAccepted = !agreements['requiresEula'];
    this.isPrivacyAccepted = !agreements['requiresPrivacyPolicy'];
  }

  updateAgreementsAndRedirectUser(agreements: object): void {
    this.updateAgreementStatus(agreements);
    // TODO: grab data for "is site setup completed" (currently hardcoded in ligin service )
    this.resolveInstallationRoute();
  }

  resolveInstallationRoute() {
    let route;

    switch (true) {
      // EULA and privacy not accepted
      case !(this.isEulaAccepted && this.isPrivacyAccepted):
        console.log('User has not accepted both EULA and Privacy, go to terms page');
        route = '/terms';
        break;
      // BRING BACK CASE FOR SETUP WHEN INSALLATION I BACK IN SCOPE
      // setup not comlpeted
      // case !this.isSetupCompleted:
      //   console.log('User has accepted both EULA and Privacy, go to installation wizard');
      //   // TODO: when API is ready, redirect to installation wizard landing
      //   // route = '/installation-wizard';
      //   route = '/home';
      //   break;
      // setup completed
      default:
        console.log('User has completed installation, go to dashboard');
        route = '/dashboard';
    }

    this.navigateToRoute(route);
  }

  navigateToRoute(route: string): void {
    this.router.navigateByUrl(route);
  }

  resetPassword(username): Observable<{}> {
    return (
      this.http
        .post(endpoints.resetPassword + username, {}, httpOptions)
        // don't catch error here, handle in the component
        .pipe(tap(_ => this.utilsService.log('reset password')))
    );
  }

  fetchUserInfo(): Observable<{}> {
    return this.http.get(endpoints.getUserInfo, httpOptions).pipe(
      tap(_ => this.utilsService.log('fetched user info')),
      catchError(this.utilsService.handleError('fetchUserInfo', []))
    );
  }
}
