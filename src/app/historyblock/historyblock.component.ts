import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'stm-app-historyblock',
  templateUrl: './historyblock.component.html',
  styleUrls: ['./historyblock.component.scss']
})
export class HistoryblockComponent implements OnInit {

  @Input() history: any;

  constructor() {
  }

  ngOnInit() {
    let item;
    // tslint:disable-next-line: forin
    for (item in this.history) {
      // this.history[item].time = moment(item.time).format('MMM D, h:mm a');
    }
  }

}
