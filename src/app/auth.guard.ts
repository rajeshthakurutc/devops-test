import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs/Observable';

import { LoginService } from './login/login.service';
import { AuthService } from './shared/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    public alertController: AlertController,
    private router: Router,
    private authService: AuthService,
    private loginService: LoginService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const token = this.authService.getAuthToken();

    // no tokens exist => user is not logged in
    if (!token) {
      console.log('no auth token');
      // any route except root (welcome) needs to redirect to login
      if (state.url !== '/') {
        this.authService.logOut();
        return false;
        // user just launched the app, we are on root (welcome), stay here
      } else {
        return true;
      }
      // tokens exist => user is logged in
    } else {
      console.log('auth token exists');
      this.authService.updateHeadersWithAuthToken(token);
      // user just launched the app, we are on root (welcome), redirect
      if (state.url === '/') {
        this.loginService.fetchAgreements().subscribe(agreements => {
          this.loginService.updateAgreementsAndRedirectUser(agreements);
        });
        return false;
      } else {
        // any route except root (welcome), stay here
        return true;
      }
    }
  }
}
