import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class NavbarService {
  private navbarClass = new BehaviorSubject('');
  navbarClass$ = this.navbarClass.asObservable();
  private itemId = new BehaviorSubject('');
  itemId$ = this.itemId.asObservable();

  constructor() {}

  updateClass(newClass: string) {
    console.log('change class to ' + newClass);
    this.navbarClass.next(newClass);
  }

  updateItemId(newId: any) {
    console.log('update id to ' + newId);
    this.itemId.next(newId);
  }
}
