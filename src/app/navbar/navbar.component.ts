import { Component, HostListener, OnInit } from '@angular/core';
import { Router, NavigationStart, ActivationEnd } from '@angular/router';
import { Location } from '@angular/common';

import { environment } from '../../environments/environment';
import { NavbarService } from './navbar.service';
import { AuthService } from '../shared/auth.service';

@Component({
  moduleId: module.id,
  selector: 'stm-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  menuVisible = false;
  pageTitle: string;
  navbarClass: string;
  navbarHideMenu = false;
  navbarRightButton: any = null;
  navbarLeftButton: any = {};
  itemId: number | string;
  appVersion: string = environment.appVersion;
  isProd: boolean = environment.production;
  reportingLink = environment.reportingLink;
  envTarget: string = environment.target;
  showLogo = false;
  firstName: string = this.authService.userInfo.givenName;
  lastName: string = this.authService.userInfo.familyName;

  constructor(
    public navbarService: NavbarService,
    private location: Location,
    private router: Router,
    private authService: AuthService
  ) {
    router.events.subscribe(event => {
      if (event instanceof ActivationEnd && (event.snapshot && !event.snapshot.children.length)) {
        const navEventData = event.snapshot.data;
        this.showLogo = navEventData.showLogo;
        this.pageTitle = navEventData.navbarTitle;
        this.navbarClass = navEventData.navbarClass;
        this.navbarHideMenu = navEventData.hideMenu;
        if (navEventData.navbarRightButton && navEventData.navbarRightButton.updatePath) {
          this.navbarService.itemId$.subscribe(data => (this.itemId = data));
        } else {
          this.itemId = null;
        }
        if (navEventData.navbarRightButton) {
          this.navbarRightButton = {
            class: navEventData.navbarRightButton.class,
            path: navEventData.navbarRightButton.path
          };
        } else {
          this.navbarRightButton = null;
        }
        if (navEventData.navbarLeftButton) {
          this.navbarLeftButton = {
            back: navEventData.navbarLeftButton.back,
            class: navEventData.navbarLeftButton.class,
            path: navEventData.navbarLeftButton.path
          };
        } else {
          this.navbarLeftButton = null;
        }
      } else if (event instanceof NavigationStart) {
        this.hideMenu();
      }
    });
  }

  ngOnInit() {
    this.navbarService.navbarClass$.subscribe(data => (this.navbarClass = data));
  }

  showMenu() {
    /***  fetch user name on menu open since init of nav happens on mount of app,
     at that time username is undefined / or app uses old cached user name */
    this.firstName = this.authService.userInfo.givenName;
    this.lastName = this.authService.userInfo.familyName;
    this.menuVisible = true;
  }

  hideMenu() {
    this.menuVisible = false;
  }

  isMenuVisible() {
    return this.menuVisible;
  }

  back() {
    this.location.back();
  }

  logout() {
    this.authService.logOut();
  }

  hideMenuWithDelay() {
    // add a slight delay or element behind menu will be clicked.
    if (this.isMenuVisible()) {
      setTimeout(() => {
        this.hideMenu();
      }, 10);
    }
  }

  @HostListener('tap', ['$event']) onTap($event) {
    $event.srcEvent.stopPropagation();
    if ($event.target.hasAttribute('closemenu')) {
      this.hideMenuWithDelay();
    }
  }

  @HostListener('swipeLeft', ['$event']) onSwipeLeft($event) {
    $event.srcEvent.stopPropagation();
    this.hideMenuWithDelay();
  }
}
