import { Injectable } from '@angular/core';

import { Gateway } from 'app/gateway';
import { Zone } from 'app/zone';
import { Monitor } from 'app/monitor';
import { GATEWAYS } from 'app/mocks/gateways';
import { ZONES } from 'app/mocks/zones';
import { MONITORS } from 'app/mocks/monitors';

@Injectable()
export class Globals {
  currentZoneIndex = 0;

  gateways: Gateway[] = GATEWAYS;
  zones: Zone[] = ZONES;
  monitors: Monitor[] = MONITORS;
}
