import { Datetime } from '@ionic/angular';
import { LowerCasePipe } from '@angular/common';
import { Zone } from 'app/zone';

export class Task {
  id: number;
  lastUpdated: string;
  created: string;
  resolved: string;
  completed: string;
  comment: string;
  description: string;
  descriptionLong: string;
  activeFlag: boolean;
  alertType: string;
  ref: Zone;
  actions: number[];
  history: any[];

  constructor(args) {
    this.id = args.id;
    this.lastUpdated = args.lastUpdated;
    this.created = args.created;
    this.resolved = args.resolved;
    this.comment = args.comment;
    this.description = args.description;
    this.descriptionLong = args.descriptionLong;
    this.activeFlag = args.activeFlag;
    this.alertType = args.alertType;
    this.ref = args.ref;
    this.actions = args.actions;
    this.history = args.history;
  }

  updateResolved(comment, action) {
    if (!action.empty()) {
      this.actions = action;
      this.activeFlag = !this.activeFlag;
    }
    if (comment != null && comment !== '') {
      this.comment = comment;
    }
  }

  addComment(comment) {
    if (comment != null && comment !== '') {
      this.comment = comment;
    }
  }
}
