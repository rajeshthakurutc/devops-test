import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { VisibilityInputComponent } from './visibility-input.component';

describe('VisibilityInputComponent', () => {
  let component: VisibilityInputComponent;
  let fixture: ComponentFixture<VisibilityInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VisibilityInputComponent],
      imports: [IonicModule.forRoot(), FormsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisibilityInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
