import { Component, ViewEncapsulation, ElementRef, forwardRef, Input, ViewChild, OnInit } from '@angular/core';
import { Icon } from '@ionic/angular';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

/* tslint:disable:no-use-before-declare */
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => VisibilityInputComponent),
  multi: true
};
/* tslint:enable:no-use-before-declare */

const noOp = () => {};

@Component({
  selector: 'stm-visibility-input',
  templateUrl: './visibility-input.component.html',
  styleUrls: ['./visibility-input.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class VisibilityInputComponent implements ControlValueAccessor, OnInit {
  @ViewChild(Icon) icon: Icon;

  @ViewChild('input') input: ElementRef;

  @Input() id: string;

  @Input() name: string;

  @Input() class: string;

  @Input() password: string;

  @Input() placeholder: string;

  innerValue: string;
  private view = false;
  type = 'password';
  private onTouchedCallback: () => void = noOp;
  onChangeCallback: (_: any) => void = noOp;

  ngOnInit() {
    this.innerValue = this.password;
  }

  changeView() {
    this.view = !this.view;
    if (this.view) {
      this.icon.name = 'eye-off';
      this.type = 'text';
    } else {
      this.icon.name = 'eye';
      this.type = 'password';
    }
    this.input.nativeElement.focus();
  }

  constructor() {}

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {}

  writeValue(value?: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }
}
