import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { VisibilityInputComponent } from '../visibility-input/visibility-input.component';

@NgModule({
  imports: [FormsModule, IonicModule ],
  exports: [VisibilityInputComponent],
  declarations: [VisibilityInputComponent],
})
export class VisibilityInputComponentModule {}
