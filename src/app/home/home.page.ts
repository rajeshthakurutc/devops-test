import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';

import { AuthService } from '../shared/auth.service';
import { DataManagerService } from '../data-manager.service';
import { LoginService } from '../login/login.service';

import { authHttpOptions, httpOptions, endpoints } from '../configs';

import { Task } from 'app/task';
import { Zone } from 'app/zone';
import { Monitor } from 'app/monitor';

@Component({
  selector: 'stm-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePageComponent implements OnInit {
  zones;
  monitors;
  gateways;
  tasks;
  sites: any = ['asd'];

  testZone: Zone;
  testMonitor: Monitor;
  testTask: Task;
  showDebug = true;

  constructor(
    private authService: AuthService,
    private dataManagerService: DataManagerService,
    private loginService: LoginService,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.authService.updateHeadersWithAuthToken(this.authService.getAuthToken());
    this.loginService.fetchAgreements().subscribe(agreements => {
      this.loginService.updateAgreementStatus(agreements);
    });
    this.dataManagerService.gateways$.subscribe(data => {
      this.gateways = data;
    });
    this.dataManagerService.zones$.subscribe(data => {
      this.zones = data;
      this.testZone = data[0];
    });
    this.dataManagerService.monitors$.subscribe(data => {
      this.monitors = data;
      this.testMonitor = data[0];
    });
    this.dataManagerService.tasks$.subscribe(data => {
      this.tasks = data;
      this.testTask = data[0];
    });
  }

  fetchSites(): Observable<{}> {
    return this.http.get(endpoints.getSites, httpOptions).pipe(
      tap(_ => console.log('fetched sites'))
      // catchError(console.error('fetchSites', []))
    );
  }

  getSites(token) {
    this.authService.updateHeadersWithAuthToken(token);
    this.fetchSites().subscribe(
      data => {
        this.sites = data;
        console.log(data);
      },
      err => {
        console.error(err);
      }
    );
  }

  updateTokens(value) {
    this.authService.validateRefreshToken(value).subscribe(data => {
      this.authService.setTokens(data);
    });
  }

  toggleDebug() {
    this.showDebug = !this.showDebug;
  }
}
