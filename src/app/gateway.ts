export class Gateway {
  id: number;
  label: string;
  serialNumber: string;

  constructor(args) {
    this.id = args.deviceId;
    this.label = args.modelName;
    this.serialNumber = args.serialNo;
  }

  updateSerialNumber(newSerialNumber: string) {
    this.serialNumber = newSerialNumber;
  }

}
