import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { ZoneConfigComponent } from './zone-config.page';

const routes: Routes = [
  {
    path: '',
    component: ZoneConfigComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ZoneConfigComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})

export class ZoneConfigModule {}
