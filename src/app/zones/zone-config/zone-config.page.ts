import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IonicModule, AlertController, ModalController } from '@ionic/angular';

import { Observable } from 'rxjs/Observable';
import { zip } from 'rxjs/observable/zip';

import { DataManagerService } from 'app/data-manager.service';
import { Zone } from 'app/zone';
import { Monitor } from 'app/monitor';

@Component({
  selector: 'stm-zone-config',
  templateUrl: './zone-config.page.html',
  styleUrls: ['./zone-config.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ZoneConfigComponent implements OnInit {
  currentSlot = -1;
  currentZoneSlots: any[] = [];
  currentZone: any;
  currentZoneIndex = 0;
  currentZoneMonitors: Monitor[] = [];

  zones: Zone[] = [];
  monitors: Monitor[] = [];
  zoneId;

  constructor(
    public alertController: AlertController,
    private router: Router,
    private route: ActivatedRoute,
    private dataManagerService: DataManagerService,
    private cd: ChangeDetectorRef
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.zoneId = parseFloat(this.route.snapshot.params['zoneId']);

    this.dataManagerService.zones$.subscribe(data => {
      this.currentZone = data.find(item => item.id === this.zoneId) || {};
      this.cd.detectChanges();
    });
    this.dataManagerService.monitors$.subscribe(data => {
      this.currentZoneMonitors = data;
      this.cd.detectChanges();
    });
  }

  fetchMonitors(zoneId) {
    this.dataManagerService.fetchMonitors(zoneId).subscribe(data => {
      this.currentZoneMonitors = this.dataManagerService.updateMonitors(data.map(item => new Monitor(item)));
      try {
        this.currentZoneSlots = Array(this.currentZone.maxMonitors - this.currentZoneMonitors.length)
          .fill(null)
          .map((item, i) => i);
      } catch (error) {
        console.error('Number of monitors is more than the ideal for this zone.');
      }
      console.log(this.currentZoneMonitors);
      this.cd.detectChanges();
    });
  }

  fetchZones() {
    this.dataManagerService.fetchZones().subscribe(data => {
      this.dataManagerService.updateZones(data.map(item => new Zone(item)));
    });
  }

  openScanner() {
    const zoneIndex = this.currentZone.id;
    console.log(this.currentZone);
    this.router.navigate(['/installation-wizard/add-device/monitor', zoneIndex], { fragment: 'zoneConfig' });
  }

  addMonitorToZone(monitor) {
    // monitor = this.getMonitorById(this.monitorInput.nativeElement.value);
    console.log('add monitor', monitor.serialNumber, 'to slot', this.currentSlot);
    // add monitor to current zone pointer at index
    this.currentZone.addMonitor(monitor, this.currentSlot);

    // update zone reference in monitor
    monitor.updateZone(this.currentZone);

    // reset slot
    this.currentSlot = -1;

    console.log(this.currentZone);
    console.log(monitor);
  }

  toggleMute(zoneId) {
    this.currentZone.muted = !this.currentZone.muted;
    alert('TODO: send notification toggle to API.');
    // TODO: API connectivity
  }

  // get list of monitor without null values
  // [null, null, null, Monitor] => [Monitor]
  getPairedMonitors(monitors: Monitor[]) {
    return monitors.filter(item => item);
  }

  async openAlert(monitorId) {
    const monitor = this.currentZoneMonitors.filter(item => item.id === monitorId)[0];

    const alert = await this.alertController.create({
      header: `Are you sure you want to remove ${monitor.label}?`,
      // inputs: this.generateAvailalbeMonitorList(),
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Cancel remove');
          }
        },
        {
          text: 'Yes',
          handler: data => {
            console.log('Confirm remove');
            this.deleteMonitor(monitorId);
            return true;
          }
        }
      ]
    });

    await alert.present();
  }

  deleteMonitor(monitorId) {
    this.dataManagerService.deleteDevice(monitorId).subscribe(
      data => {
        this.fetchMonitors(this.zoneId);
      },
      error => {
        alert('Monitor could not be removed, please try again.');
        console.error(error);
      }
    );
  }
}
