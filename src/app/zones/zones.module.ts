import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ConcentricCircleComponent } from 'app/concentric-circle/concentric-circle.component';
import { ZoneTileComponent } from './zone-tile/zone-tile.component';
import { ZoneDetailComponent } from './zone-detail/zone-detail.component';
import { ZonesComponent } from './zones.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [ZoneTileComponent, ConcentricCircleComponent, ZonesComponent, ZoneDetailComponent],
  exports: [ZonesComponent, ZoneTileComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZonesModule {}
