import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';

import { Zone } from 'app/zone';
import { LoaderService } from 'app/loader/loader.service';
import { ZonesService } from 'app/zones/zones.service';
import { SiteSwitcherService } from '../site-switcher/site-switcher.service';

@Component({
  selector: 'stm-zones',
  templateUrl: './zones.component.html',
  styleUrls: ['./zones.component.scss']
})
export class ZonesComponent implements OnInit, OnDestroy {
  constructor(
    private zonesService: ZonesService,
    private siteSwitcherService: SiteSwitcherService,
    private cd: ChangeDetectorRef,
    public loaderService: LoaderService
  ) {}

  zones: Zone[];
  subscribe;
  zoneIntervalSubscribe;
  showNoZonesCopy = false;
  interval: NodeJS.Timer;

  ngOnInit() {
    this.getZonesFromObservable();
    this.refreshZonesOnInterval();
  }

  getZonesFromObservable() {
    this.subscribe = this.zonesService.zones$.subscribe(zones => {
      this.zones = zones;
      this.showNoZones(zones);
      this.cd.detectChanges();
    });
  }

  showNoZones(zones) {
    this.showNoZonesCopy = zones.length === 0 ? true : false;
  }

  refreshZonesOnInterval() {
    this.interval = setInterval(() => {
      const siteId = this.siteSwitcherService.getSiteId();
      // in rare case user doesn't have a site, prevent fetch of zones
      if (!!siteId) {
        this.zoneIntervalSubscribe = this.zonesService.fetchZonesBySiteId(siteId).subscribe(zones => {
          console.log('UPDATE ZONES ON INTERVAL');
          this.zonesService.updateZonesObservable(zones);
          this.showNoZones(zones);
        });
      }
    }, 60000);
  }

  ngOnDestroy() {
    // clear setInterval
    clearInterval(this.interval);
    // unsubscribe from all zones observables
    this.subscribe.unsubscribe();
    // in case user logs out before interval kicks off check if subscription is defined
    if (this.zoneIntervalSubscribe) {
      this.zoneIntervalSubscribe.unsubscribe();
    }
    // reset $zones observable array
    this.zonesService.updateZonesObservable([]);
  }
}
