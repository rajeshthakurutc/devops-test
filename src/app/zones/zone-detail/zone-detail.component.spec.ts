import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { TimeAgoPipe } from 'ngx-moment';

import { ZoneDetailComponent } from './zone-detail.component';
import { GraphComponent } from '../../graph/graph.component';
import { NavbarService } from '../../navbar/navbar.service';
import { Zone } from '../../zone';

describe('ZoneDetailComponent', () => {
  let component: ZoneDetailComponent;
  let fixture: ComponentFixture<ZoneDetailComponent>;
  const testZone = new Zone({
    id: 47,
    siteId: 19,
    name: 'My Freezer',
    type: 'Freezer',
    tempType: 'Frozen',
    highContinuousCritical: 1,
    highContinuousWarning: 1,
    lowContinuousCritical: 1,
    lowContinuousWarning: 1,
    maxCriticalTemperature: 38,
    maxWarningTemperature: 36,
    minCriticalTemperature: 30,
    minWarningTemperature: 31.9,
    normalOperations: 1,
    status: 'Critical'
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GraphComponent, ZoneDetailComponent, TimeAgoPipe],
      imports: [IonicModule.forRoot(), HttpClientModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        NavbarService,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: {},
              paramMap: {
                get: () => 1
              }
            }
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneDetailComponent);
    component = fixture.componentInstance;
    component.zone = testZone;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
