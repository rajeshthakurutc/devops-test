import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NavbarService } from 'app/navbar/navbar.service';
import { ZonesService } from 'app/zones/zones.service';
import { Zone } from 'app/zone';
import { Monitor } from 'app/monitor';
import { Task } from 'app/task';

@Component({
  selector: 'stm-zone-detail',
  templateUrl: './zone-detail.component.html',
  styleUrls: ['./zone-detail.component.scss']
})
export class ZoneDetailComponent implements OnInit, OnDestroy {
  id: number;
  zones: Zone[];
  zone: {};
  monitors: Monitor[];
  tasks: Task[];
  dataSets: any[];
  activity: any[];
  interval: NodeJS.Timer;
  subscribe;
  status: string;

  constructor(
    private zonesService: ZonesService,
    private navbarService: NavbarService,
    private router: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) {
    this.id = +this.router.snapshot.paramMap.get('id');
    this.navbarService.updateItemId(this.id);
  }

  ngOnInit() {
    this.fetchZone();
    this.refreshZoneDataOnInterval();
  }

  refreshZoneDataOnInterval() {
    this.interval = setInterval(() => {
      this.fetchZone();
    }, 60000);
  }

  fetchZone() {
    this.subscribe = this.zonesService.fetchZoneDetail(this.id).subscribe(
      zone => {
        this.zone = zone;
        this.status = this.zonesService.setZoneStatusCopy(zone);
        this.zonesService.updateZoneDetailObservable(zone);
        this.cd.detectChanges();
      },
      err => {
        console.log('zone detail fetch error', err);
      }
    );
  }

  ngOnDestroy() {
    // clear setInterval
    clearInterval(this.interval);
    // unsubscribe from observable
    this.subscribe.unsubscribe();
    console.log('zone detail DESTOROYED');
  }

  //   this.dataManagerService.monitors$.subscribe(data => {
  //     this.monitors = data.filter(item => item.zoneId === this.id);

  //     // Note: some values are hardcoded for now, later they'll need to be wired up
  //     this.dataSets = [];
  //     this.monitors.forEach((item) => {
  //       this.dataSets.push({
  //         name: item.label,
  //         values: [ 10.1, 13.9, 13, 14.8, 14.5, 12, 13.5, 13, 14, 17, null ],
  //         legend: {
  //           colorName: 'red',
  //           label: '17°F↑'
  //         }
  //       });
  //     });
  //   });

  //   this.activity = [
  //       {
  //         what: 'Temperature excursion',
  //         when: '30m ago',
  //         details: 'Active 30m ago.',
  //         actionNeeded: true,
  //       },
  //       {
  //         what: 'Notification disabled',
  //         when: 'Jan 03, 2019',
  //         details: '',
  //         actionNeeded: undefined,
  //       },
  //       {
  //         what: 'Temperature excursion',
  //         when: 'Dec 23, 2018',
  //         details: 'Warning range for 30m.',
  //         actionNeeded: false,
  //       },
  //       {
  //         what: 'Device issue',
  //         when: 'Dec 21, 2018',
  //         details: 'Disconnected for 3 hrs.',
  //         actionNeeded: undefined,
  //       },
  //       {
  //         what: 'Temperature excursion',
  //         when: 'Dec 15, 2018',
  //         details: 'Critical range for 1h 2m.',
  //         actionNeeded: undefined,
  //       },
  //       {
  //         what: 'Notification disabled',
  //         when: 'Dec 10, 2018',
  //         details: '',
  //         actionNeeded: undefined,
  //       },
  //       {
  //         what: 'Notification disabled',
  //         when: 'Dec 09, 2018',
  //         details: '',
  //         actionNeeded: undefined,
  //       },
  //       {
  //         what: 'Temperature excursion',
  //         when: 'Dec 07, 2018',
  //         details: 'Critical range for 2h 11m.',
  //         actionNeeded: undefined,
  //       },
  //     ];
  // }
}
