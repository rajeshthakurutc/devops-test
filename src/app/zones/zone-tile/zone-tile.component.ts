import { Component, OnInit, Input } from '@angular/core';
import { Zone } from 'app/zone';
import { ZonesService } from 'app/zones/zones.service';

@Component({
  selector: 'stm-zone-tile',
  templateUrl: './zone-tile.component.html',
  styleUrls: ['./zone-tile.component.scss']
})
export class ZoneTileComponent implements OnInit {
  constructor(private zonesService: ZonesService) {}
  @Input() zone;
  numberOfMonitors: string;
  status: string;

  ngOnInit() {
    this.numberOfMonitors =
      this.zone.numMonitors === 1 ? `${this.zone.numMonitors} Monitor` : `${this.zone.numMonitors} Monitors`;
    this.status = this.zonesService.setZoneStatusCopy(this.zone);
  }
}
