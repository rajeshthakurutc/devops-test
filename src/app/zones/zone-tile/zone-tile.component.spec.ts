import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { ZoneTileComponent } from './zone-tile.component';
import { ZonesService } from '../zones.service';
import { Zone } from '../../zone';

describe('ZoneTileComponent', () => {
  let component: ZoneTileComponent;
  let fixture: ComponentFixture<ZoneTileComponent>;
  const testZone = new Zone({ numMonitors: 0 });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [ZoneTileComponent],
      providers: [ZonesService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneTileComponent);
    component = fixture.componentInstance;
    component.zone = testZone;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
