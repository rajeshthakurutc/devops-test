import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, tap, map, finalize } from 'rxjs/operators';
/* tslint:disable-next-line:import-blacklist */
import { Observable, BehaviorSubject } from 'rxjs';

import { UtilsService } from '../shared/utils.service';
import { httpOptions, endpoints } from '../configs';
import { Zone } from 'app/zone';

@Injectable({
  providedIn: 'root'
})
export class ZonesService {
  constructor(private http: HttpClient, private utilsService: UtilsService) {}

  private zones = new BehaviorSubject([]);
  private zoneDetail = new BehaviorSubject(Zone);
  zones$ = this.zones.asObservable();
  zoneDetail$ = this.zoneDetail.asObservable();

  fetchZonesBySiteId(siteId: number): Observable<Zone[]> {
    const path = `${siteId}/zone`;
    return this.http.get<Zone[]>(endpoints.getSites + path, httpOptions).pipe(
      tap(_ => this.utilsService.log('fetched zones by site ID')),
      map(zones => {
        // sort zones alphabetically by name
        zones.sort(this.sortOn('name'));
        return zones;
      }),
      catchError(this.utilsService.handleError('fetchZonesBySiteId', []))
    );
  }

  updateZonesObservable(zones) {
    this.zones.next(zones);
  }

  updateZoneDetailObservable(zone) {
    this.zoneDetail.next(zone);
  }

  fetchZoneDetail(id: number): Observable<Zone> {
    return this.http
      .get<any>(endpoints.getZone + id, httpOptions)
      .pipe(tap(_ => this.utilsService.log('fetched zone')));
  }

  sortOn(property: string) {
    return (a, b) => {
      const propertyA = a[property].trim().toLowerCase();
      const propertyB = b[property].trim().toLowerCase();

      if (propertyA < propertyB) {
        return -1;
      } else if (propertyA > propertyB) {
        return 1;
      } else {
        return 0;
      }
    };
  }

  setZoneStatusCopy(zone: Zone) {
    let status: string;
    switch (zone.status) {
      case 'InstallationRequired':
        status = 'Setup Zone';
        break;
      case 'WaitingForData':
        status = 'Waiting for data...';
        break;
      default:
        status = zone.status;
        break;
    }
    return status;
  }
}
