import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { ZonesService } from './zones.service';

describe('ZonesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ZonesService]
    });
  });

  it('should be created', inject([ZonesService], (service: ZonesService) => {
    expect(service).toBeTruthy();
  }));

  it('should sort zone names correctly', () => {
    const service: ZonesService = TestBed.get(ZonesService);
    const zones = [
      {
        'name': ' z am i waiting'
      },
      {
        'name': 'My Freezer'
      }
    ];
    const sortedZones = zones.sort(service.sortOn('name'));
    expect(zones[0]['name']).toBe('My Freezer', 'm comes before z irrespective of leading whitespace');
    expect(zones[1]['name']).toBe(' z am i waiting', 'z comes after m irrespective of leading whitespace');
  });
});
