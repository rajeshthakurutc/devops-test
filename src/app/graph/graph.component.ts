import { Component, OnInit, Renderer2, Input } from '@angular/core';
import { Chart } from 'chart.js';
import 'chartjs-plugin-annotation';

// Style points
Chart.defaults.global.elements.point.radius = 0;
Chart.defaults.global.elements.point.hoverRadius = 5;
// Disable all chart animations
Chart.defaults.global.animation = false;

const hoverColor = '#555';

Chart.defaults.STMLine = Chart.defaults.line;
Chart.controllers.STMLine = Chart.controllers.line.extend({
  draw(ease) {
    Chart.controllers.line.prototype.draw.call(this, ease);

    // Draw vertical tooltip line
    if (this.chart.tooltip._active && this.chart.tooltip._active.length) {
      const activePoint = this.chart.tooltip._active[0],
        ctx = this.chart.ctx,
        x = activePoint.tooltipPosition().x,
        topY = this.chart.scales['y-axis-0'].top,
        bottomY = this.chart.scales['y-axis-0'].bottom;

      ctx.save();
      ctx.beginPath();
      ctx.moveTo(x, topY);
      ctx.lineTo(x, bottomY);
      ctx.lineWidth = 1;
      ctx.strokeStyle = hoverColor;
      ctx.stroke();
      ctx.restore();
    }

    // Draw vertical line for the chart's most recent value
    /*
      (() => {
        const ctx = this.chart.ctx,
              x = 100, // XXX TODO find the most recent point time-wise
              topY = this.chart.scales['y-axis-0'].top,
              bottomY = this.chart.scales['y-axis-0'].bottom;

        ctx.save();
        ctx.beginPath();
        ctx.moveTo(x, topY);
        ctx.lineTo(x, bottomY);
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#2C3E50';
        ctx.stroke();
        ctx.restore();
      })();
      */
  }
});

@Component({
  moduleId: module.id,
  selector: 'stm-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit {
  @Input() datasets: any[] = [];
  @Input() thresholds: [number, number, number, number] = [15, 10, -10, -15];

  chartInstance: Chart;
  gradients: CanvasGradient[] = [];
  isLoading: boolean;
  context: CanvasRenderingContext2D = null;
  verticalRangeScale = 30;
  rgbColors = [
    [235, 22, 22], // Red
    [255, 153, 2], // Orange
    [52, 203, 129] // Green
  ];
  chartColors = {
    red_solid: this.rgba(this.rgbColors[0][0], this.rgbColors[0][1], this.rgbColors[0][2], 1),
    orange_solid: this.rgba(this.rgbColors[1][0], this.rgbColors[1][1], this.rgbColors[1][2], 1),
    green_solid: this.rgba(this.rgbColors[2][0], this.rgbColors[2][1], this.rgbColors[2][2], 1),
    red_shade: this.rgba(this.rgbColors[0][0], this.rgbColors[0][1], this.rgbColors[0][2], 0.2),
    orange_shade: this.rgba(this.rgbColors[1][0], this.rgbColors[1][1], this.rgbColors[1][2], 0.2),
    green_shade: this.rgba(this.rgbColors[2][0], this.rgbColors[2][1], this.rgbColors[2][2], 0.2),
    red_transparent: this.rgba(this.rgbColors[0][0], this.rgbColors[0][1], this.rgbColors[0][2], 0),
    orange_transparent: this.rgba(this.rgbColors[1][0], this.rgbColors[1][1], this.rgbColors[1][2], 0),
    line_red: '#f5b7b8',
    line_orange: '#fddfb5',
    line_green: '#c9f0dc'
  };

  constructor(private renderer: Renderer2) {}

  ngOnInit(): void {
    const canvasElement = this.renderer.selectRootElement('canvas');
    this.context = canvasElement.getContext('2d');

    this.renderChart();
  }

  isProperColorStopValue(value: number): boolean {
    return value >= 0 && value <= 1;
  }

  toggleDataset(event, datasetIndex: number): void {
    if (this.chartInstance) {
      const chartDatasetProps = this.chartInstance.getDatasetMeta(datasetIndex);
      chartDatasetProps.hidden = !chartDatasetProps.hidden;
      this.chartInstance.update();

      const visibilityStatusClassName = 'hidden';
      const targetElement = event.target.tagName === 'A' ? event.target : event.target.parentElement;
      const methodName = chartDatasetProps.hidden ? 'add' : 'remove';
      targetElement.classList[methodName](visibilityStatusClassName);
    }
  }

  renderChart(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }

    this.chartInstance = new Chart(this.context, {
      type: 'STMLine',
      data: {
        labels: ['7d ago', '', '', '', '', '3d ago', '', '', '', '', 'Now'],
        datasets: (() => {
          return this.datasets.map(dataSet => {
            return {
              label: dataSet.name,
              lineTension: 0,
              backgroundColor: 'transparent',
              borderWidth: 1,
              data: dataSet.values.slice(),
              pointHoverBackgroundColor: hoverColor,
              pointBorderColor: 'transparent',
              fill: 'start'
            };
          });
        })()
      },
      options: {
        maintainAspectRatio: false,
        multiTooltipTemplate: self => {
          return self.label[self.datasetLabel] + ': ' + self.value;
        },
        tooltips: {
          intersect: true,
          mode: 'x-axis',
          custom(tooltip) {
            if (tooltip) {
              // Hide the color box;
              tooltip.displayColors = false;
            }
          },
          callbacks: {
            label(tooltipItem, data) {
              const datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
              return datasetLabel + ' : ' + tooltipItem.yLabel + '℉';
            },
            // Remove title
            title: () => {}
          }
        },
        hover: {
          mode: 'index',
          intersect: false
        },
        title: {
          display: true
        },
        legend: {
          display: false
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                drawBorder: false,
                color: '#F7F7F7',
                drawTicks: false
              },
              ticks: {
                padding: 8,
                autoSkip: false,
                fontColor: '#A6A6A6',
                fontFamily: 'Libre Franklin',
                fontSize: 9,
                minRotation: 0,
                maxRotation: 0
              }
            }
          ],
          yAxes: [
            {
              position: 'right',
              gridLines: {
                drawBorder: false,
                color: '#F7F7F7',
                drawTicks: false
              },
              ticks: {
                padding: 8,
                fontColor: '#A6A6A6',
                fontFamily: 'Libre Franklin',
                fontSize: 9,
                suggestedMax: this.verticalRangeScale,
                suggestedMin: -this.verticalRangeScale
              }
            }
          ]
        },
        annotation: {
          drawTime: 'beforeDatasetsDraw',
          annotations: [
            {
              type: 'line',
              mode: 'horizontal',
              scaleID: 'y-axis-0',
              value: this.thresholds[1],
              borderColor: this.chartColors.line_orange,
              borderWidth: 1
            },
            {
              type: 'line',
              mode: 'horizontal',
              scaleID: 'y-axis-0',
              value: this.thresholds[0],
              borderColor: this.chartColors.line_red,
              borderWidth: 1
            },
            {
              type: 'line',
              mode: 'horizontal',
              scaleID: 'y-axis-0',
              value: '0',
              borderColor: this.chartColors.line_green,
              borderWidth: 1
            },
            {
              type: 'line',
              mode: 'horizontal',
              scaleID: 'y-axis-0',
              value: this.thresholds[3],
              borderColor: this.chartColors.line_orange,
              borderWidth: 1
            },
            {
              type: 'line',
              mode: 'horizontal',
              scaleID: 'y-axis-0',
              value: this.thresholds[2],
              borderColor: this.chartColors.line_red,
              borderWidth: 1
            }
          ]
        }
      },
      plugins: [
        {
          afterLayout: (chart, options) => {
            const scale = chart.scales['y-axis-0'];
            const maxLabelValY = scale.max;

            // Change every dataset's background color to the gradient
            chart.data.datasets.forEach(dataset => {
              // STEP 1 determine which color the graph line should be
              const minValue = Math.min(...dataset.data);
              const maxValue = Math.max(...dataset.data);
              let gradientType = 0;
              dataset.borderColor = this.chartColors.green_solid;
              if (maxValue >= this.thresholds[1] || minValue <= this.thresholds[2]) {
                dataset.borderColor = this.chartColors.orange_solid;
                gradientType++;

                if (maxValue >= this.thresholds[0] || minValue <= this.thresholds[3]) {
                  dataset.borderColor = this.chartColors.red_solid;
                  gradientType++;
                }
              }

              // STEP 2 determine which gradient this dataset should have
              if (gradientType > 0) {
                // Create a linear gradient with the dimentions of the scale
                const gradient = chart.ctx.createLinearGradient(0, scale.bottom, 0, scale.top);

                const thresholdGradient = maxValue / maxLabelValY / 2 + 0.5;

                if (this.isProperColorStopValue(thresholdGradient)) {
                  // ▅ Transparent red/orange starts (from the bottom)
                  gradient.addColorStop(0, this.chartColors[(gradientType === 1 ? 'orange' : 'red') + '_transparent']);
                  // ⬆ Red/orange stops (at the highest point of the chart)
                  gradient.addColorStop(
                    thresholdGradient,
                    this.chartColors[(gradientType === 1 ? 'orange' : 'red') + '_shade']
                  );
                }

                dataset.backgroundColor = gradient;
              }
            });
          }
        }
      ]
    });
  }

  rgba(red: number, green: number, blue: number, alpha: number): string {
    return 'rgba(' + red + ', ' + green + ', ' + blue + ', ' + alpha + ')';
  }
}
