import { environment } from '../../environments/environment';

const HOST = environment.API_HOST;
const authApiRoot = HOST + 'api/auth/';
const securityApiRoot = HOST + 'api/security/';
const adminApiRoot = HOST + 'api/stm/admin/';
const siteApiRoot = HOST + 'api/site/';

const endpoints = {
  // authentication
  authenticateUser: authApiRoot + 'connect/token',

  // EULA, Privacy, password
  getAgreements: securityApiRoot + 'Agreements',
  acceptEula: securityApiRoot + 'Agreements/Eula',
  acceptPrivacy: securityApiRoot + 'Agreements/PrivacyPolicy',
  resetPassword: securityApiRoot + 'Password/ResetPassword/',
  getUserInfo: securityApiRoot + 'User/MyInfo/',
  getSecurityPolicy: securityApiRoot + 'organization/{organizationId}/SecurityPolicy',
  updatePassword: securityApiRoot + 'Password/ChangePassword',

  // main app data
  getSession: adminApiRoot + 'session',
  getSetupStatus: adminApiRoot + 'setup/{storeId}',
  updateStoreSetupStep: adminApiRoot + 'setup/store/{storeId}/step',
  completeStoreSetup: adminApiRoot + 'setup/{storeId}/completed',
  getDevices: adminApiRoot + 'devices',
  getGateways: adminApiRoot + 'devices/gateways?storeId={storeId}',
  getMonitors: adminApiRoot + 'devices/monitors?storeId={storeId}',
  getAllMonitors: adminApiRoot + 'devices/monitors',
  getZones: adminApiRoot + 'zones?storeId={storeId}',
  createMonitor: adminApiRoot + 'devices/monitors',
  createGateway: adminApiRoot + 'devices/gateways',
  getSites: siteApiRoot + 'site/',
  getZone: siteApiRoot + 'zone/',
  // below are calls to mock data (API is down, testing, etc.)
  // getSession: '/assets/data/session.json',
  // getSetupStatus: '/assets/data/setup.json',
  // getGateways: '/assets/data/gateways.json',
  // getZones: '/assets/data/zones.json',
  // getMonitors: '/assets/data/monitors.json',
  getTasks: '/assets/data/tasks.json'
};

export default endpoints;
