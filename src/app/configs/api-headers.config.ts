import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

// hardcoding until API is updated to not use stm_session
const stm_session = 'ABCD-6';

const controlOptions = {
  'Access-Control-Allow-Origin': 'Origin, X-Requested-With, Content-Type, Accept',
  'Access-Control-Allow-Headers': '*',
  'cache-control': 'no-cache',
  'Ocp-Apim-Subscription-Key': environment.API_SUBSCRIPTION_KEY
};

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: '',
    stm_session,
    ...controlOptions
  })
};

const authHttpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    Authorization: 'Basic Y29tbW9uX2NsaWVudF9qd3Q6c2VjcmV0',
    ...controlOptions
  })
};

export { authHttpOptions, httpOptions };
