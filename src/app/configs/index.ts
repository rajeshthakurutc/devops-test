import { authHttpOptions, httpOptions } from './api-headers.config';
import endpoints from './endpoints.config';

export { endpoints, authHttpOptions, httpOptions };
