import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'stm-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: [
    // '../help.page.scss',
    './contact-us.component.scss',
  ]
})
export class ContactUsComponent implements OnInit {

  emailAddress = 'STMSupport@sensitech.com';
  emailSubject = 'STM Support';
  emailBody = 'I need help with …';

  constructor(
  ) {
  }

  ngOnInit() {
  }

  emailLink(): string {
    return 'mailto:' + this.emailAddress +
      '?subject=' + encodeURIComponent(this.emailSubject) +
      '&body=' + encodeURIComponent(this.emailBody);
  }

}
