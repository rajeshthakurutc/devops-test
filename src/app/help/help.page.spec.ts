import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Globals } from 'app/globals';
import { RouterTestingModule } from '@angular/router/testing';

import { HelpPageComponent } from './help.page';
import { NavbarService } from '../navbar/navbar.service';

describe('HelpPageComponent', () => {
  let component: HelpPageComponent;
  let fixture: ComponentFixture<HelpPageComponent>;
  let globalsSpy;

  beforeEach(async(() => {
    globalsSpy = jasmine.createSpyObj('Globals', ['']);

    TestBed.configureTestingModule({
      declarations: [ HelpPageComponent ],
      imports: [
        RouterTestingModule.withRoutes([]),
      ],
      providers: [
        NavbarService,
        { provide: Globals, useValue: globalsSpy },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
