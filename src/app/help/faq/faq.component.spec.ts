import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { Globals } from 'app/globals';

import { FAQComponent } from './faq.component';

describe('ContactUsComponent', () => {
  let component: FAQComponent;
  let fixture: ComponentFixture<FAQComponent>;
  let globalsSpy;

  beforeEach(async(() => {
    globalsSpy = jasmine.createSpyObj('Globals', ['']);
    TestBed.configureTestingModule({
      declarations: [ FAQComponent ],
      imports: [ IonicModule.forRoot() ],
      providers: [
        { provide: Globals, useValue: globalsSpy }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FAQComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
