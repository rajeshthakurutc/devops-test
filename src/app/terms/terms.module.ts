import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { TermsPageComponent } from './terms.page';
import { TermsService } from './terms.service';

const routes: Routes = [
  {
    path: '',
    component: TermsPageComponent
  }
];

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes)],
  declarations: [TermsPageComponent],
  providers: [TermsService]
})
export class TermsPageModule {}
