import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../shared/auth.service';
import { TermsService } from './terms.service';

@Component({
  selector: 'stm-terms',
  templateUrl: './terms.page.html',
  styleUrls: ['./terms.page.scss']
})
export class TermsPageComponent implements OnInit {
  constructor(private router: Router, private authService: AuthService, private termsService: TermsService) {}

  ngOnInit() {}

  dismiss() {
    this.authService.logOut();
  }

  accept() {
    this.termsService.acceptEulaAndPrivacy().subscribe(
      () => {
        this.router.navigateByUrl('/dashboard');
      },
      () => {
        // the API errored out on acceoting the terms
        // log the user out and redirect to login
        alert('There was an issue accepting the EULA and Privacy Notice. Please log in and try again.');
        // clear tokens and navigate to login
        this.authService.logOut();
      }
    );
  }
}
