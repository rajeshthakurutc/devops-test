import { Injectable } from '@angular/core';
/* tslint:disable-next-line:import-blacklist */
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { HttpClient } from '@angular/common/http';
import { httpOptions, endpoints } from '../configs';
import { UtilsService } from '../shared/utils.service';

@Injectable()
export class TermsService {
  constructor(private http: HttpClient, private utils: UtilsService) {}

  acceptEulaAndPrivacy(): Observable<{}> {
    const eula = this.http.post(endpoints.acceptEula, {}, httpOptions).pipe(
      tap(_ => this.utils.log('accepted EULA'))
      // don't catch error here, handle in the component
    );

    const privacy = this.http.post(endpoints.acceptPrivacy, {}, httpOptions).pipe(
      tap(_ => this.utils.log('accept privacy'))
      // don't catch error here, handle in the component
    );

    return forkJoin([eula, privacy]);
  }
}
