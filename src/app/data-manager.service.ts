import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { httpOptions, endpoints } from './configs';

/* tslint:disable-next-line:import-blacklist */
import { Observable, BehaviorSubject, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Gateway } from 'app/gateway';
import { Zone } from 'app/zone';
import { Monitor } from 'app/monitor';
import { Task } from 'app/task';

@Injectable({
  providedIn: 'root'
})
export class DataManagerService {
  private installStepsUser = ['TermsOfService', 'Notifications', 'Profile'];
  private installStepsStore = {
    KitReceived: 'KitReceived',
    ConfirmDevices: 'ConfirmDevices',
    GatewaySetupSuccessful: 'GatewaySetupSuccessful'
  };

  storeId: Observable<number>;
  idealGateways: number;
  idealMonitors: number;

  isEulaAccepted: boolean;
  isPrivacyAccepted: boolean;
  isSetupCompleted = false;

  private gateways = new BehaviorSubject([]);
  gateways$ = this.gateways.asObservable();
  private monitors = new BehaviorSubject([]);
  monitors$ = this.monitors.asObservable();
  private zones = new BehaviorSubject([]);
  zones$ = this.zones.asObservable();
  private tasks = new BehaviorSubject([]);
  tasks$ = this.tasks.asObservable();

  constructor(private http: HttpClient) {}

  private log(message, fail?) {
    const bg = fail ? '#eb1616' : '#62cc99';
    console.log(`%cAPI${fail ? ' fail' : ''}` + `%c ${message}`, `background:${bg};color:#fff;padding:2px 5px`, '');
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // alert(`Failed to load data (${operation})`)
      console.error(error);
      this.log(`${operation} failed: ${error.message}`, true);
      return of(result as T);
    };
  }

  getStoreId() {
    return this.storeId;
  }

  setStoreId(storeId) {
    this.storeId = storeId;
  }

  updateEndpoints(storeId) {
    // tslint:disable-next-line: forin
    for (const key in endpoints) {
      endpoints[key] = endpoints[key].replace(/\{storeId\}/g, storeId);
    }
    console.log('updated endpoints with storeId', storeId);
  }

  updateGateways(gateways) {
    this.gateways.next(gateways);
  }

  updateMonitors(monitors) {
    this.monitors.next(monitors);
    return monitors;
  }

  updateZones(zones) {
    this.zones.next(zones);
    return zones;
  }

  updateTasks(tasks) {
    this.tasks.next(tasks);
    return tasks;
  }

  updateIdealGateways(n) {
    return (this.updateIdealGateways = n);
  }

  updateIdealMonitors(n) {
    return (this.updateIdealMonitors = n);
  }

  updateSetupCompleted(status) {
    return (this.isSetupCompleted = status);
  }

  fetchSetupStatus(): Observable<{}> {
    console.log('get setup status');
    return this.http.get(endpoints.getSetupStatus, httpOptions).pipe(
      tap(_ => this.log('fetched setup status')),
      catchError(this.handleError('fetchSetupStatus', []))
    );
  }

  fetchSession() {
    console.log('get session');
    const req = this.http.get(endpoints.getSession, httpOptions);
    req.subscribe(data => {
      this.updateEndpoints(data['currentStoreId']);
    });
    return req;
  }

  fetchDevices(): Observable<any[]> {
    console.log('get devices');
    return this.http.get<any[]>(endpoints.getDevices, httpOptions).pipe(
      tap(_ => this.log(`fetched ${_.length} devices`)),
      catchError(this.handleError('fetchDevices', []))
    );
  }

  fetchGateways(): Observable<Gateway[]> {
    console.log('get gateways for storeId', this.storeId);
    return this.http.get<Gateway[]>(endpoints.getGateways, httpOptions).pipe(
      tap(_ => this.log(`fetched ${_.length} gateways`)),
      catchError(this.handleError('fetchGateways', []))
    );
  }

  fetchZones(): Observable<Zone[]> {
    console.log('get zones for storeId', this.storeId);
    return this.http.get<Zone[]>(endpoints.getZones, httpOptions).pipe(
      tap(_ => this.log(`fetched ${_.length} zones`)),
      catchError(this.handleError('fetchZones', []))
    );
  }

  fetchMonitors(zoneId?): Observable<Monitor[]> {
    console.log('get monitors for storeId', this.storeId);
    let url = endpoints.getMonitors;
    if (zoneId) {
      url += `&zoneId=${zoneId}`;
    }
    return this.http.get<Monitor[]>(url, httpOptions).pipe(
      tap(_ => this.log(`fetched ${_.length} monitors`)),
      catchError(this.handleError('fetchMonitors', []))
    );
  }

  fetchTasks(): Observable<Task[]> {
    console.log('get tasks');
    return this.http.get<Task[]>(endpoints.getTasks, httpOptions).pipe(
      tap(_ => this.log(`fetched ${_.length} tasks`)),
      catchError(this.handleError('fetchTasks', []))
    );
  }

  createMonitor(newMonitor): Observable<{}> {
    return (
      this.http
        .post<Monitor>(endpoints.createMonitor, newMonitor, httpOptions)
        // don't catch error here, handle in the component
        .pipe(tap(_ => this.log(`created new monitor ${_[0]}`)))
    );
  }

  createGateway(newGateway): Observable<{}> {
    return (
      this.http
        .post<Gateway>(endpoints.createGateway, newGateway, httpOptions)
        // don't catch error here, handle in the component
        .pipe(tap(_ => this.log(`created new gateway ${_[0]}`)))
    );
  }

  getAllMonitors(): Observable<{}> {
    return this.http.get<Monitor[]>(endpoints.getAllMonitors, httpOptions).pipe(
      tap(_ => this.log(`${_.length} total monitors`)),
      catchError(this.handleError('getAllMonitors', []))
    );
  }

  deleteDevice(deviceId) {
    const url = endpoints.getDevices + '/' + deviceId;
    return (
      this.http
        .delete<{}>(url, httpOptions)
        // don't catch error here, handle in the component
        .pipe(
          tap(_ => this.log(`removed device with id ${deviceId}`))
          // catchError(this.handleError('deleteDevice', []))
        )
    );
  }

  updateInstallationStep(update) {
    const url = endpoints.updateStoreSetupStep;
    return this.http.put<{}>(url, update, httpOptions).pipe(
      tap(_ => this.log(`udpated step ${update.stepId}`)),
      catchError(this.handleError('updateInstallationStep', []))
    );
  }

  completeInstallation() {
    const url = endpoints.completeStoreSetup;
    return this.http.put<{}>(url, {}, httpOptions).pipe(
      tap(_ => this.log(`completed store installation ${_}`)),
      catchError(this.handleError('completeInstallation', []))
    );
  }
}
