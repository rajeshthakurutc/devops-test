import { TestBed, async, inject } from '@angular/core/testing';
import { AlertController, ModalController, AngularDelegate } from '@ionic/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

import { AuthGuard } from './auth.guard';

describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        AlertController,
        ModalController,
        AngularDelegate,
      ],
      imports: [
        RouterTestingModule.withRoutes([]),
        HttpClientModule,
      ],
    });
  });

  it('should ...', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
