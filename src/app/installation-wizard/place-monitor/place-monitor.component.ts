import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { DataManagerService } from 'app/data-manager.service';
import { Zone } from 'app/zone';
import { Monitor } from 'app/monitor';

@Component({
  selector: 'stm-place-monitor',
  templateUrl: './place-monitor.component.html',
  styleUrls: ['../installation-wizard.page.scss', './place-monitor.component.scss']
})
export class PlaceMonitorComponent implements OnInit {
  zoneId: number;
  monitorSerial: string;
  zones: Zone[] = [];
  monitors: Monitor[] = [];
  currentZone: Zone;
  isZoneConfig = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private dataManagerService: DataManagerService
  ) {}

  ngOnInit() {
    this.zoneId = parseFloat(this.route.snapshot.params['zoneId']);
    this.monitorSerial = this.route.snapshot.params['monitorSerial'];
    this.isZoneConfig = this.route.snapshot.fragment === 'zoneConfig';

    this.dataManagerService.zones$.subscribe(data => {
      this.zones = data;
      this.currentZone = this.zones.filter(item => item.id === this.zoneId)[0];
      console.log(this.currentZone);
    });
  }

  back() {
    this.location.back();
  }

  placeMonitor() {
    const newMonitor = [
      {
        serialNo: this.monitorSerial,
        deviceType: 'monitor',
        modelName: 'test model name',
        zoneId: this.zoneId,
        storeId: this.dataManagerService.getStoreId()
      }
    ];

    this.dataManagerService.createMonitor(newMonitor).subscribe(
      monitorData => {
        forkJoin(this.dataManagerService.fetchZones(), this.dataManagerService.fetchMonitors()).subscribe(data => {
          this.dataManagerService.updateZones(data[0].map(item => new Zone(item)));
          this.dataManagerService.updateMonitors(data[1].map(item => new Monitor(item)));
          if (this.isZoneConfig) {
            this.router.navigate(['/', 'zones', 'zone-config', this.zoneId]);
          } else {
            this.router.navigate(['/', 'installation-wizard', 'zone-setup', this.zoneId]);
          }
        });
      },
      error => {
        if (error.status === 409) {
          console.warn('This serial number has already been scanned.');
        }

        if (this.isZoneConfig) {
          this.router.navigate(['/', 'zones', 'zone-config', this.zoneId]);
        } else {
          this.router.navigate(['/', 'installation-wizard', 'zone-setup', this.zoneId]);
        }
      }
    );
  }
}
