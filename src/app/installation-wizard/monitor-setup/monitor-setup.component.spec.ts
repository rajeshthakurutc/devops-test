import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

import { MonitorSetupComponent } from './monitor-setup.component';

describe('MonitorSetupComponent', () => {
  let component: MonitorSetupComponent;
  let fixture: ComponentFixture<MonitorSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitorSetupComponent ],
      imports: [
        RouterTestingModule.withRoutes([]),
        HttpClientModule,
        IonicModule.forRoot(),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitorSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
