import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Slides } from '@ionic/angular';

import { DataManagerService } from 'app/data-manager.service';

@Component({
  selector: 'stm-monitor-setup',
  templateUrl: './monitor-setup.component.html',
  styleUrls: ['../installation-wizard.page.scss', './monitor-setup.component.scss']
})
export class MonitorSetupComponent implements OnInit {
  @ViewChild(Slides) slider: Slides;

  sliderOptions: object = {
    spaceBetween: 32
  };

  isLastSlide = false;

  constructor(private router: Router, private dataManagerService: DataManagerService) {}

  // udpate last slide after promise
  // https://github.com/ionic-team/ionic/issues/14918
  slideChanged() {
    this.slider.isEnd().then(data => {
      this.isLastSlide = data;
    });
  }

  ngOnInit() {}

  goToZoneSetup(): void {
    this.dataManagerService.zones$.subscribe(data => {
      if (!data[0]) {
        return;
      }

      this.router.navigate(['installation-wizard', 'zone-setup', data[0].id], { replaceUrl: true });
    });
  }
}
