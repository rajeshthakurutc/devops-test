import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataManagerService } from 'app/data-manager.service';
import { Gateway } from 'app/gateway';
import { Zone } from 'app/zone';
import { Monitor } from 'app/monitor';

@Component({
  selector: 'stm-zone-summary',
  templateUrl: './zone-summary.component.html',
  styleUrls: [
    '../installation-wizard.page.scss',
    './zone-summary.component.scss'
  ]
})
export class ZoneSummaryComponent implements OnInit {

  gateways: Gateway[];
  zones: Zone[];
  monitors: Monitor[];

  constructor(
    private dataManagerService: DataManagerService,
    private router: Router
  ) { }

  ngOnInit() {
    this.dataManagerService.gateways$.subscribe(data => this.gateways = data);
    this.dataManagerService.monitors$.subscribe(data => this.monitors = data);
    this.dataManagerService.zones$.subscribe(data => this.zones = data);
  }

  // get list of monitor without null values
  // [null, null, null, Monitor] => [Monitor]
  getPairedMonitors(monitors: Monitor[]) {
    return monitors.filter((item) => item);
  }

  next() {
    this.dataManagerService.completeInstallation().subscribe(data => {
      this.router.navigateByUrl('dashboard');
    });
  }

}
