import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { Globals } from 'app/globals';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

import { ZoneSummaryComponent } from './zone-summary.component';

describe('ZoneSummaryComponent', () => {
  let component: ZoneSummaryComponent;
  let fixture: ComponentFixture<ZoneSummaryComponent>;
  let globalsSpy;

  beforeEach(async(() => {
    globalsSpy = jasmine.createSpyObj('Globals', ['']);
    TestBed.configureTestingModule({
      declarations: [ ZoneSummaryComponent ],
      imports: [
        RouterTestingModule.withRoutes([]),
        IonicModule.forRoot(),
        HttpClientModule,
      ],
      providers: [
        { provide: Globals, useValue: globalsSpy }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
