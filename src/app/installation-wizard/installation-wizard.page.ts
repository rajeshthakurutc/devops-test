import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataManagerService } from 'app/data-manager.service';

@Component({
  selector: 'stm-installation-wizard',
  templateUrl: './installation-wizard.page.html',
  styleUrls: ['./installation-wizard.page.scss'],
})

export class InstallationWizardPageComponent implements OnInit {

  constructor(
    public dataManagerService: DataManagerService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  next() {
    const update = {
      stepId: 'KitReceived',
      status: 1
    };
    this.dataManagerService.updateInstallationStep(update).subscribe(data => {
      this.router.navigateByUrl('/installation-wizard/kit-confirmation');
    });
  }

}
