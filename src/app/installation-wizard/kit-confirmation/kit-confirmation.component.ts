import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { DataManagerService } from 'app/data-manager.service';
import { Gateway } from 'app/gateway';
import { Zone } from 'app/zone';
import { Monitor } from 'app/monitor';

@Component({
  selector: 'stm-kit-confirmation',
  templateUrl: './kit-confirmation.component.html',
  styleUrls: [
    '../installation-wizard.page.scss',
    './kit-confirmation.component.scss'
  ]
})

export class KitConfirmationComponent implements OnInit {

  gateways: Gateway[];
  zones: Zone[];
  monitors: Monitor[];

  idealGateways: number;
  idealMonitors: number;

  constructor(
    private location: Location,
    private dataManagerService: DataManagerService,
    private router: Router
  ) { }

  ngOnInit() {
    this.dataManagerService.gateways$.subscribe(data => this.gateways = data);
    this.dataManagerService.monitors$.subscribe(data => this.monitors = data);
    this.dataManagerService.zones$.subscribe(data => {
      this.idealGateways = this.dataManagerService.idealGateways;
      this.idealMonitors = this.dataManagerService.idealMonitors;
      this.zones = data;
    });

  }

  next() {
    const update = {
      stepId: 'ConfirmDevices',
      status: 1
    };
    this.dataManagerService.updateInstallationStep(update).subscribe(data => {
      this.router.navigateByUrl('/installation-wizard/scan-gateway');
    });
  }

}
