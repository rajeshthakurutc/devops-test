import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { Location } from '@angular/common';
import { Globals } from 'app/globals';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

import { KitConfirmationComponent } from './kit-confirmation.component';

describe('KitConfirmationComponent', () => {
  let component: KitConfirmationComponent;
  let fixture: ComponentFixture<KitConfirmationComponent>;
  let locationSpy, globalsSpy;

  beforeEach(async(() => {
    globalsSpy = jasmine.createSpyObj('Globals', ['']);
    locationSpy = jasmine.createSpyObj('Location', ['back']);

    TestBed.configureTestingModule({
      declarations: [ KitConfirmationComponent ],
      imports: [
        IonicModule.forRoot(),
        HttpClientModule,
        RouterTestingModule.withRoutes([]),
      ],
      providers: [
        { provide: Location, useValue: locationSpy },
        { provide: Globals, useValue: globalsSpy }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KitConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
