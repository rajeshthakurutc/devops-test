import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule, ModalController, AlertController } from '@ionic/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { Globals } from 'app/globals';
import { HttpClientModule } from '@angular/common/http';

import { ZoneSetupComponent } from './zone-setup.page';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { VisibilityInputComponent } from 'app/visibility-input/visibility-input.component';
import { FormsModule } from '@angular/forms';

const globals = {
  'gateways': [
    {'id': 123, 'label': 'Gateway', 'serialNumber': 'A1234567890'}
  ],
  'zones': [
    { 'id': 1, 'label': 'Refrigerator', 'monitors': [], 'maxMonitors': 2, 'installationStatus': null, 'operationStatus': null },
    { 'id': 2, 'label': 'Cooler', 'monitors': [], 'maxMonitors': 3, 'installationStatus': null, 'operationStatus': null },
    { 'id': 3, 'label': 'Dine Line', 'monitors': [], 'maxMonitors': 1, 'installationStatus': null, 'operationStatus': null },
    { 'id': 4, 'label': 'Dine Reach', 'monitors': [], 'maxMonitors': 5, 'installationStatus': null, 'operationStatus': null }
  ],
  'monitors': [
    { 'id': 1, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567001', 'zone': null },
    { 'id': 2, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567002', 'zone': null },
    { 'id': 3, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567003', 'zone': null },
    { 'id': 4, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567004', 'zone': null },
    { 'id': 5, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567005', 'zone': null },
    { 'id': 6, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567006', 'zone': null },
    { 'id': 7, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567007', 'zone': null },
    { 'id': 8, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567008', 'zone': null },
    { 'id': 9, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567009', 'zone': null },
    { 'id': 10, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567010', 'zone': null },
    { 'id': 11, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567011', 'zone': null },
    { 'id': 12, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567012', 'zone': null },
    { 'id': 13, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567013', 'zone': null },
    { 'id': 14, 'label': 'Temperature Monitor', 'serialNumber': 'A1234567014', 'zone': null }
  ]
};

describe('ZoneSetupComponent', () => {
  let component: ZoneSetupComponent;
  let fixture: ComponentFixture<ZoneSetupComponent>;
  let alertControllerSpy, modalControllerSpy;

  beforeEach(async(() => {
    alertControllerSpy = jasmine.createSpyObj('AlertController', ['create']);
    modalControllerSpy = jasmine.createSpyObj('ModalController', ['create', 'didDismiss', 'present']);

    TestBed.configureTestingModule({
      declarations: [ ZoneSetupComponent, VisibilityInputComponent ],
      imports: [
        RouterTestingModule.withRoutes([]),
        IonicModule.forRoot(),
        FormsModule,
        HttpClientModule,
      ],
      providers: [
        { provide: AlertController, useValue: alertControllerSpy },
        { provide: ModalController, useValue: modalControllerSpy },
        { provide: Globals, useValue: globals }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
