import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';

import { zip } from 'rxjs/observable/zip';

import { DataManagerService } from 'app/data-manager.service';
import { Zone } from 'app/zone';
import { Monitor } from 'app/monitor';

@Component({
  selector: 'stm-zone-setup',
  templateUrl: './zone-setup.page.html',
  styleUrls: ['../installation-wizard.page.scss', './zone-setup.page.scss']
})
export class ZoneSetupComponent implements OnInit {
  currentSlot = -1;
  currentZoneSlots: any[] = [];
  currentZone: Zone;
  currentZoneIndex = 0;
  currentZoneMonitors: Monitor[] = [];

  zones: Zone[] = [];
  monitors: Monitor[] = [];
  zoneId;

  constructor(
    public alertController: AlertController,
    private router: Router,
    private route: ActivatedRoute,
    private dataManagerService: DataManagerService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {}
  //   this.zoneId = parseFloat(this.route.snapshot.params['zoneId']);

  //   // subscribe to both data streams
  //   zip(this.dataManagerService.zones$, this.dataManagerService.monitors$).subscribe(data => {
  //     // if no monitors/zones found, data is not loaded; exit
  //     if (!data[0].length || !data[1].length) {
  //       return;
  //     }

  //     this.zones = data[0];
  //     this.monitors = data[1];
  //     this.currentZoneIndex = this.zones.findIndex(item => item.id === this.zoneId);
  //     this.currentZone = this.zones[this.currentZoneIndex];
  //     this.currentSlot = -1;

  //     if (!this.currentZone) {
  //       console.error('Zone does not exist for this store.');
  //       return;
  //     }

  //     this.currentZoneMonitors = this.monitors.filter(item => item.zoneId === this.currentZone.id);

  //     console.log(this.currentZone);
  //     console.log(this.currentZoneMonitors);
  //     try {
  //       this.currentZoneSlots = Array(this.currentZone.maxMonitors - this.currentZoneMonitors.length)
  //         .fill(null)
  //         .map((item, i) => i);
  //     } catch (error) {
  //       console.error('Number of monitors is more than the ideal for this zone.');
  //     }
  //   });
  // }

  // fetchMonitors() {
  //   this.dataManagerService.fetchMonitors().subscribe(data => {
  //     this.dataManagerService.updateMonitors(data.map(item => new Monitor(item)));
  //   });
  // }

  // fetchZones() {
  //   this.dataManagerService.fetchZones().subscribe(data => {
  //     this.dataManagerService.updateZones(data.map(item => new Zone(item)));
  //   });
  // }

  // nextZone() {
  //   // determine zone installation status
  //   let installationStatus;
  //   if (this.getPairedMonitors(this.currentZoneMonitors).length === this.currentZone.maxMonitors) {
  //     installationStatus = 0;
  //   } else if (this.getPairedMonitors(this.currentZoneMonitors).length < this.currentZone.maxMonitors) {
  //     installationStatus = 1;
  //   } else {
  //     installationStatus = 2;
  //   }
  //   this.currentZone.updateInstallationStatus(installationStatus);

  //   // if last zone in list, route to next page and exit
  //   if (this.currentZoneIndex + 1 >= this.zones.length) {
  //     this.router.navigateByUrl('/installation-wizard/zone-summary');
  //     // return;
  //   } else {
  //     this.router.navigateByUrl(`/installation-wizard/zone-setup/${this.zones[this.currentZoneIndex + 1].id}`);
  //   }
  // }

  // openScanner(zoneIndex, slot) {
  //   this.router.navigate(['/installation-wizard/add-device/monitor', zoneIndex]);
  // }

  // addMonitorToZone(monitor) {
  //   // monitor = this.getMonitorById(this.monitorInput.nativeElement.value);
  //   console.log('add monitor', monitor.serialNumber, 'to slot', this.currentSlot);
  //   // add monitor to current zone pointer at index
  //   this.currentZone.addMonitor(monitor, this.currentSlot);

  //   // update zone reference in monitor
  //   monitor.updateZone(this.currentZone);

  //   // reset slot
  //   this.currentSlot = -1;

  //   console.log(this.currentZone);
  //   console.log(monitor);
  // }

  // // get list of monitor without null values
  // // [null, null, null, Monitor] => [Monitor]
  // getPairedMonitors(monitors: Monitor[]) {
  //   return monitors.filter(item => item);
  // }

  // // async openAlert(slot) {
  // //   this.currentSlot = slot;
  // //
  // //   const alert = await this.alertController.create({
  // //     header: 'Select Monitor',
  // //     inputs: this.generateAvailalbeMonitorList(),
  // //     buttons: [
  // //       {
  // //         text: 'Cancel',
  // //         role: 'cancel',
  // //         cssClass: 'secondary',
  // //         handler: () => {
  // //           console.log('Confirm Cancel');
  // //         }
  // //       }, {
  // //         text: 'Ok',
  // //         handler: (data) => {
  // //           console.log('Confirm Ok', data);
  // //           if (!data) {
  // //             return false
  // //           }
  // //           this.addMonitorToZone(data);
  // //         }
  // //       }
  // //     ]
  // //   });
  // //
  // //   await alert.present();
  // // }

  // deleteDevice(deviceId) {
  //   this.dataManagerService.deleteDevice(deviceId).subscribe(data => {
  //     console.log(data);
  //     alert('deleted device with id ' + deviceId);
  //   });
  // }
}
