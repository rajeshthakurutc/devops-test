import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { ZoneSetupComponent } from './zone-setup.page';

const routes: Routes = [
  {
    path: '',
    component: ZoneSetupComponent
  }
];

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes)],
  declarations: [ZoneSetupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZoneSetupModule {}
