import { Component, OnInit, ViewChild } from '@angular/core';
import { Slides } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Gateway } from 'app/gateway';

import { DataManagerService } from 'app/data-manager.service';
import { NavbarService } from 'app/navbar/navbar.service';

@Component({
  selector: 'stm-gateway-setup',
  templateUrl: './gateway-setup.component.html',
  styleUrls: ['../installation-wizard.page.scss', './gateway-setup.component.scss']
})
export class GatewaySetupComponent implements OnInit {
  @ViewChild(Slides) slider: Slides;

  sliderOptions: object = {
    spaceBetween: 32
  };

  isLastSlide = false;
  currentSlide: number;
  deviceSerialNumber = '';

  constructor(
    private dataManagerService: DataManagerService,
    private navbarService: NavbarService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  // udpate last slide after promise
  // https://github.com/ionic-team/ionic/issues/14918
  slideChanged() {
    this.slider.isEnd().then(data => {
      this.isLastSlide = data;
    });
    this.slider.getActiveIndex().then(data => {
      this.currentSlide = data;
    });
  }

  ngOnInit() {
    this.deviceSerialNumber = this.route.snapshot.params['deviceSerialNumber'];
  }
  nextSlide() {
    if (this.isLastSlide) {
      this.navbarService.updateClass('bg-blue-dark');
    } else {
      this.slider.slideNext();
    }
  }

  addGateWayAndContinue() {
    const update = {
      stepId: 'GatewaySetupSuccessful',
      status: 1
    };

    const newGateway = [
      {
        serialNo: this.deviceSerialNumber,
        deviceType: 'gateway',
        modelName: 'test model name',
        zoneId: null,
        storeId: this.dataManagerService.getStoreId()
      }
    ];

    this.dataManagerService.createGateway(newGateway).subscribe(
      () => {
        this.dataManagerService.updateGateways([new Gateway(newGateway)]);
        this.dataManagerService.updateInstallationStep(update).subscribe(() => {
          this.router.navigateByUrl('/installation-wizard/monitor-setup');
        });
      },
      error => {
        // for odd chance someone else at a different store scans a device w/ the same serial number
        if (error.status === 409) {
          window.alert('This gateway serial number was already scanned.');
        } else {
          console.warn('create gateway error: ', error);
        }
      }
    );
  }
}
