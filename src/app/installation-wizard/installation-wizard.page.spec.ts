import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Globals } from 'app/globals';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

import { InstallationWizardPageComponent } from './installation-wizard.page';

describe('InstallationWizardPage', () => {
  let component: InstallationWizardPageComponent;
  let fixture: ComponentFixture<InstallationWizardPageComponent>;
  let globalsSpy;

  beforeEach(async(() => {
    globalsSpy = jasmine.createSpyObj('Globals', ['']);

    TestBed.configureTestingModule({
      declarations: [ InstallationWizardPageComponent ],
      imports: [
        HttpClientModule,
        RouterTestingModule.withRoutes([]),
      ],
      providers: [{ provide: Globals, useValue: globalsSpy }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstallationWizardPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
