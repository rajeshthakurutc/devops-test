import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataManagerService } from 'app/data-manager.service';
import { Gateway } from 'app/gateway';

@Component({
  selector: 'stm-scan-gateway',
  templateUrl: './scan-gateway.component.html',
  styleUrls: [
    '../installation-wizard.page.scss',
    './scan-gateway.component.scss'
  ]
})
export class ScanGatewayComponent implements OnInit {

  gateways: Gateway[];

  constructor(
    private dataManagerService: DataManagerService,
    private router: Router
  ) {}

  ngOnInit() {
    this.dataManagerService.gateways$.subscribe(data => this.gateways = data);
  }

  continue() {
    this.router.navigateByUrl('/installation-wizard/add-device/gateway');
  }

}
