import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'stm-zone-instructions',
  templateUrl: './zone-instructions.component.html',
  styleUrls: [
    '../installation-wizard.page.scss',
    './zone-instructions.component.scss'
  ]
})
export class ZoneInstructionsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
