import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { DataManagerService } from 'app/data-manager.service';
import { Gateway } from 'app/gateway';
import { Zone } from 'app/zone';
import { Monitor } from 'app/monitor';

@Component({
  selector: 'stm-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['../installation-wizard.page.scss', './add-device.component.scss']
})
export class AddDeviceComponent implements OnInit {
  deviceSerialNumber = '';
  options = {
    formats: 'CODE_39'
  };
  gateways: Gateway[];
  zones: Zone[];
  monitors: Monitor[];
  deviceType: string;
  duplicateSerial = false;
  isZoneConfig = false;

  constructor(
    private barcodeScanner: BarcodeScanner,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private dataManagerService: DataManagerService
  ) {
    this.stringInput = this.stringInput.bind(this);
  }

  ngOnInit() {
    this.dataManagerService.gateways$.subscribe(data => (this.gateways = data));
    this.dataManagerService.monitors$.subscribe(data => (this.monitors = data));
    this.dataManagerService.zones$.subscribe(data => (this.zones = data));
    this.deviceType = this.route.snapshot.params.zoneId ? 'monitor' : 'gateway';
    this.isZoneConfig = this.route.snapshot.fragment === 'zoneConfig';
  }

  isOkToContinue(): boolean {
    return this.deviceSerialNumber.length === 10;
  }

  scan() {
    this.barcodeScanner
      .scan(this.options)
      .then(barcodeData => {
        if (!barcodeData.cancelled) {
          const text = barcodeData.text.replace(/\+/g, ''); // remove pluses
          this.deviceSerialNumber = text.substr(0, 10);
        }
      })
      .catch(errMsg => {
        if (errMsg === 'Access to the camera has been prohibited; please enable it in the Settings app to continue.') {
          alert('This app has no access to the camera. Please enable it in the Settings app to continue.');
        }

        console.log('Unable to scan barcode:', errMsg);
      });
  }

  stringInput($event) {
    this.deviceSerialNumber = $event.target.value || '';
  }

  submitMonitorCode() {
    if (this.isOkToContinue()) {
      switch (this.deviceType) {
        case 'monitor':
          const zoneId: number = parseFloat(this.route.snapshot.params['zoneId']);
          const currentZone: Zone = this.zones.filter(item => item.id === zoneId)[0];

          this.dataManagerService.fetchDevices().subscribe(data => {
            console.log(data);
            if (data.filter(item => item.serialNo.toLowerCase() === this.deviceSerialNumber.toLowerCase()).length > 0) {
              this.duplicateSerial = true;
            } else {
              this.duplicateSerial = false;
              if (this.isZoneConfig) {
                this.router.navigate(['/installation-wizard/place-monitor', zoneId, this.deviceSerialNumber], {
                  fragment: 'zoneConfig'
                });
              } else {
                this.router.navigateByUrl(`installation-wizard/place-monitor/${zoneId}/${this.deviceSerialNumber}`);
              }
            }
          });
          break;

        case 'gateway':
          /**fetch devices (gets both monitors and gateways) here as well
            because both monitor and gateways  have to have unique serial #'s */

          this.dataManagerService.fetchDevices().subscribe(
            data => {
              if (
                data.filter(item => item.serialNo.toLowerCase() === this.deviceSerialNumber.toLowerCase()).length > 0
              ) {
                this.duplicateSerial = true;
              } else {
                this.duplicateSerial = false;
                //  go to gateway setup instructions
                this.router.navigate(['/installation-wizard/gateway-setup', this.deviceSerialNumber]);
              }
            },
            err => {
              console.warn('Gateway fetchDevices error: ', err);
            }
          );
          break;

        default:
          break;
      }
    }
  }
  back() {
    this.location.back();
  }
}
