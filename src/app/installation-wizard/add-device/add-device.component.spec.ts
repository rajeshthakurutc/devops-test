import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { BarcodeScannerMock } from '../../../../mocks/barcode-scanner-mock';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Globals } from 'app/globals';
import { HttpClientModule } from '@angular/common/http';

import { AddDeviceComponent } from './add-device.component';

describe('AddDeviceComponent', () => {
  let component: AddDeviceComponent;
  let fixture: ComponentFixture<AddDeviceComponent>;
  let mockZoneId: string;
  let mockMonitorId: string;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      declarations: [AddDeviceComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        Globals,
        { provide: BarcodeScanner, useClass: BarcodeScannerMock },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: {
                zoneId: this.mockZoneId,
                monitorId: this.mockMonitorId
              }
            }
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    mockZoneId = '1';
    mockMonitorId = '2';
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have modifiable monitor serial number variable', () => {
    expect(component.deviceSerialNumber).toMatch('');

    const mockMonitorSerialNumber = 'KL7BR7NK70';
    const mockEventObject = {
      target: {
        value: mockMonitorSerialNumber
      }
    };
    component.stringInput(mockEventObject);
    expect(component.deviceSerialNumber).toMatch(mockMonitorSerialNumber);

    const mockEmptyMonitorSerialNumber = '';
    const mockEmptyEventObject = {
      target: {
        value: mockEmptyMonitorSerialNumber
      }
    };
    component.stringInput(mockEmptyEventObject);
    expect(component.deviceSerialNumber).toMatch(mockEmptyMonitorSerialNumber);
  });

  it('should validate monitor serial number', () => {
    expect(component.isOkToContinue()).toBeFalsy();

    component.deviceSerialNumber = 'KL7BR7N';
    expect(component.isOkToContinue()).toBeFalsy();

    component.deviceSerialNumber = 'KL7BR7NK70';
    expect(component.isOkToContinue()).toBeTruthy();
  });

  it('should let user scan monitor serial number using camera', done => {
    component.scan();

    setTimeout(() => {
      expect(component.deviceSerialNumber).toMatch('KL7BR7');
      done();
    }, 500);
  });
});
