import { Injectable } from '@angular/core';
import { tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient, HttpRequest } from '@angular/common/http';
/* tslint:disable-next-line:import-blacklist */
import { Observable, BehaviorSubject, of } from 'rxjs';

import { Md5 } from 'ts-md5/dist/md5';

import { httpOptions, authHttpOptions, endpoints } from '../configs';
import { AnalyticsService } from '../shared/analytics.service';
import { UtilsService } from '../shared/utils.service';
import { SiteSwitcherService } from '../site-switcher/site-switcher.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public authToken: string = localStorage.getItem('authToken');
  public refreshToken: string = localStorage.getItem('refreshToken');
  public userInfo = this.getUserInfo();

  cachedRequests: Array<HttpRequest<any>> = [];

  constructor(
    private http: HttpClient,
    private router: Router,
    private utilsService: UtilsService,
    public analytics: AnalyticsService,
    private siteSwitcherService: SiteSwitcherService
  ) {}

  getAuthToken(): string {
    return this.authToken;
  }

  getRefreshToken(): string {
    return this.refreshToken;
  }

  getUserInfo(): any {
    this.userInfo = JSON.parse(localStorage.getItem('userInfo')) || {};
    return this.userInfo;
  }

  setAuthToken(token: string) {
    localStorage.setItem('authToken', token);
    this.authToken = token;
    this.updateHeadersWithAuthToken(token);
  }

  setRefreshToken(token: string) {
    localStorage.setItem('refreshToken', token);
    this.refreshToken = token;
  }

  setUserInfo(userInfo: any) {
    localStorage.setItem('userInfo', JSON.stringify(userInfo));
    this.userInfo = userInfo;
  }

  clearAuthToken() {
    localStorage.removeItem('authToken');
    this.authToken = '';
  }

  clearRefreshToken() {
    localStorage.removeItem('refreshToken');
    this.refreshToken = '';
  }

  clearUserInfo() {
    localStorage.removeItem('userInfo');
    this.userInfo = {};
  }

  setTokens(data) {
    const { access_token, refresh_token, id_token }: any = data;
    // id_token is returned from api when refresh token expires
    this.setAuthToken(access_token || id_token);
    this.setRefreshToken(refresh_token);
  }

  validateRefreshToken(token): Observable<{}> {
    const formData = `grant_type=refresh_token&scope=rtweb.api offline_access&refresh_token=${token}`;
    return (
      this.http
        .post(endpoints.authenticateUser, formData, authHttpOptions)
        // don't catch error here, handle in the component
        .pipe(tap(_ => this.utilsService.log('updated refresh token')))
    );
  }

  clearAuthAndRefreshTokens(): void {
    this.clearAuthToken();
    this.clearRefreshToken();
  }

  updateHeadersWithAuthToken(token: string) {
    httpOptions.headers = httpOptions.headers.set('Authorization', 'Bearer ' + token);
  }

  setHashedUserForAppInsights(userName: string) {
    this.analytics.track('setAuthenticatedUserContext', [Md5.hashStr(userName), null, true]);
  }

  logOut(): void {
    // clear tokens and navigate to login
    this.router.navigateByUrl('/login');
    this.clearAuthAndRefreshTokens();
    this.clearUserInfo();
    this.siteSwitcherService.clearSavedSite();
    this.analytics.track('clearAuthenticatedUserContext');
  }

  fetchSecurityPolicy(): Observable<{}> {
    return this.http.get(endpoints.getSecurityPolicy, httpOptions).pipe(
      tap(_ => this.utilsService.log('fetched security policy')),
      catchError(this.utilsService.handleError('fetchSecurityPolicy', []))
    );
  }

  updatePassword(): Observable<{}> {
    return this.http.post(endpoints.updatePassword, httpOptions).pipe(
      tap(_ => this.utilsService.log('updated passsword')),
      catchError(this.utilsService.handleError('updatePassword', []))
    );
  }
}
