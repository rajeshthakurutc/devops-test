import { Injectable } from '@angular/core';
import { AppInsights } from 'applicationinsights-js';

import { UtilsService } from './utils.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {
  constructor(private utils: UtilsService) {
    AppInsights.downloadAndSetup({ instrumentationKey: environment.INSTRUMENTATION_KEY });
    AppInsights.queue.push(() => {
      AppInsights.context.addTelemetryInitializer(envelope => {
        envelope.tags['app.role'] = 'stm-ui';
      });
    });
  }

  track(fn, args?) {
    if (AppInsights && AppInsights[fn] instanceof Function) {
      this.utils.log([fn, JSON.stringify(args)], false, true);
      AppInsights[fn].apply(AppInsights, args);
    }
  }
}
