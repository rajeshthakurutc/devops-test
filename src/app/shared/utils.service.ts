import { Injectable } from '@angular/core';
/* tslint:disable-next-line:import-blacklist */
import { Observable, of } from 'rxjs';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  constructor() {}

  log(message, fail?, analytics?) {
    const bg = fail ? '#eb1616' : analytics ? '#80438e' : '#62cc99';
    const title = analytics ? 'track' : 'API';
    console.log(
      `%c${title}${fail ? ' fail' : ''}` + `%c ${message}`,
      `background:${bg};color:#fff;padding:2px 5px`,
      ''
    );
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.log(`${operation} failed: ${error.message}`, true);
      return of(result as T);
    };
  }
}
