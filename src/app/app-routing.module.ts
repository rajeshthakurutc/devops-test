import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './auth.guard';
import { KitConfirmationComponent } from './installation-wizard/kit-confirmation/kit-confirmation.component';
import { ScanGatewayComponent } from './installation-wizard/scan-gateway/scan-gateway.component';
import { ZoneInstructionsComponent } from './installation-wizard/zone-instructions/zone-instructions.component';
import { GatewaySetupComponent } from './installation-wizard/gateway-setup/gateway-setup.component';
import { MonitorSetupComponent } from './installation-wizard/monitor-setup/monitor-setup.component';
import { PlaceMonitorComponent } from './installation-wizard/place-monitor/place-monitor.component';
import { ZoneSummaryComponent } from './installation-wizard/zone-summary/zone-summary.component';
import { CorrectiveActionComponent } from './tasks/corrective-action/corrective-action.component';
import { AddDeviceComponent } from './installation-wizard/add-device/add-device.component';
import { TaskDetailComponent } from './tasks/task-detail/task-detail.component';
import { SuccessScreenComponent } from './tasks/success-screen/success-screen.component';
import { ZoneDetailComponent } from './zones/zone-detail/zone-detail.component';
import { FAQComponent } from './help/faq/faq.component';
import { ContactUsComponent } from './help/contact-us/contact-us.component';
import { MyProfileComponent } from './settings/my-profile/my-profile.component';
import { NotificationManagementComponent } from './settings/notification-management/notification-management.component';
import { SystemLogsComponent } from './settings/system-logs/system-logs.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LicenseComponent } from './terms/license/license.component';
import { PrivacyComponent } from './terms/privacy/privacy.component';
import { AboutComponent } from './about/about.component';

const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent,
    canActivate: [AuthGuard],
    data: {
      navbarLeftButton: null,
      hideMenu: true
    }
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginPageModule',
    data: {
      navbarLeftButton: null,
      hideMenu: true
    }
  },
  {
    path: 'home',
    canActivate: [AuthGuard],
    loadChildren: './home/home.module#HomePageModule',
    data: {
      navbarTitle: 'Secret Menu',
      navbarRightButton: null
    }
  },
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardPageModule',
        data: {
          navbarTitle: null,
          showLogo: true
        }
      },
      {
        path: 'create-profile',
        loadChildren: './create-profile/create-profile.module#CreateProfilePageModule',
        data: {
          navbarLeftButton: {
            back: true
          }
        }
      },
      {
        path: 'installation-wizard',
        children: [
          {
            path: '',
            loadChildren: './installation-wizard/installation-wizard.module#InstallationWizardModule',
            data: {
              navbarLeftButton: null,
              navbarClass: 'bg-blue-light'
            }
          },
          {
            path: 'kit-confirmation',
            component: KitConfirmationComponent,
            data: {
              navbarLeftButton: {
                back: true
              },
              navbarClass: 'bg-blue-light'
            }
          },
          {
            path: 'scan-gateway',
            component: ScanGatewayComponent,
            data: {
              navbarLeftButton: {
                back: true
              },
              navbarClass: 'bg-blue-light'
            }
          },
          {
            path: 'gateway-setup/:deviceSerialNumber',
            component: GatewaySetupComponent,
            data: {
              navbarLeftButton: {
                back: true
              },
              navbarClass: 'bg-blue-light'
            }
          },
          {
            path: 'monitor-setup',
            component: MonitorSetupComponent,
            data: {
              navbarLeftButton: {
                back: true
              },
              navbarClass: 'bg-blue-light'
            }
          },
          {
            path: 'zone-instructions',
            component: ZoneInstructionsComponent
          },
          {
            path: 'zone-summary',
            component: ZoneSummaryComponent,
            data: {
              navbarLeftButton: {
                back: true
              },
              navbarClass: 'bg-blue-dark'
            }
          },
          {
            path: 'add-device/gateway',
            component: AddDeviceComponent,
            data: {
              navbarLeftButton: {
                back: true
              },
              navbarClass: 'bg-blue-light'
            }
          },
          {
            path: 'add-device/monitor/:zoneId',
            component: AddDeviceComponent,
            data: {
              navbarLeftButton: {
                back: true
              },
              navbarClass: 'bg-blue-light'
            }
          },
          {
            path: 'place-monitor/:zoneId/:monitorSerial',
            component: PlaceMonitorComponent,
            data: {
              navbarLeftButton: {
                back: true
              },
              navbarClass: 'bg-blue-light'
            }
          }
        ]
      },
      {
        path: 'installation-wizard/zone-setup/:zoneId',
        loadChildren: './installation-wizard/zone-setup/zone-setup.module#ZoneSetupModule',
        data: {
          navbarLeftButton: {
            back: true
          }
        }
      },
      {
        path: 'terms',
        children: [
          {
            path: '',
            loadChildren: './terms/terms.module#TermsPageModule',
            data: {
              navbarTitle: 'EULA and Privacy Notice',
              navbarClass: 'eula-page',
              navbarLeftButton: null,
              hideMenu: true
            }
          },
          {
            path: 'license',
            component: LicenseComponent,
            data: {
              navbarTitle: 'License Agreement',
              navbarRightButton: null,
              navbarLeftButton: {
                back: true
              }
            }
          },
          {
            path: 'privacy',
            component: PrivacyComponent,
            data: {
              navbarTitle: 'Privacy Notice',
              navbarRightButton: null,
              navbarLeftButton: {
                back: true
              }
            }
          }
        ]
      },
      {
        path: 'tasks',
        children: [
          {
            path: '',
            loadChildren: './tasks/tasks-view/tasks-view.module#TasksViewPageModule',
            data: {
              navbarTitle: 'Tasks'
            }
          },
          {
            path: 'corrective-action/:id',
            component: CorrectiveActionComponent,
            data: {
              navbarTitle: 'Task Detail',
              navbarRightButton: null,
              navbarLeftButton: {
                back: true
              }
            }
          },
          {
            path: 'task-detail/:id',
            component: TaskDetailComponent,
            data: {
              navbarTitle: 'Task Detail',
              navbarRightButton: null,
              navbarLeftButton: {
                back: true
              }
            }
          },
          {
            path: 'success-screen/:id',
            component: SuccessScreenComponent,
            data: {
              navbarTitle: 'Task Detail',
              navbarRightButton: null
            }
          }
        ],
        data: {
          navbarTitle: 'Tasks',
          navbarRightButton: {
            class: 'dashboard',
            path: '/dashboard'
          }
        }
      },

      {
        path: 'zone-detail/:id',
        component: ZoneDetailComponent,
        data: {
          navbarTitle: 'Zone Detail',
          navbarLeftButton: {
            back: true
          }
          // navbarRightButton: {
          //   class: 'config',
          //   path: '/zones/zone-config',
          //   updatePath: true
          // }
        }
      },
      {
        path: 'zone-config/:zoneId',
        loadChildren: './zones/zone-config/zone-config.module#ZoneConfigModule',
        data: {
          navbarTitle: 'Zone Configuration',
          navbarLeftButton: {
            back: true
          }
        }
      },

      {
        path: 'settings',
        children: [
          {
            path: '',
            loadChildren: './settings/settings.module#SettingsModule',
            data: {
              navbarTitle: 'Settings'
            }
          },
          {
            path: 'my-profile',
            component: MyProfileComponent,
            data: {
              navbarTitle: 'My Profile',
              navbarLeftButton: {
                back: true
              }
            }
          },
          {
            path: 'notification-management',
            component: NotificationManagementComponent,
            data: {
              navbarTitle: 'Notifications',
              navbarLeftButton: {
                back: true
              }
            }
          },
          {
            path: 'system-logs',
            component: SystemLogsComponent,
            data: {
              navbarTitle: 'System Logs',
              navbarLeftButton: {
                back: true
              }
            }
          },
          {
            path: 'change-password',
            loadChildren: './change-password/change-password.module#ChangePasswordPageModule',
            data: {
              navbarTitle: 'Change Password',
              navbarLeftButton: {
                back: true
              },
              navbarRightButton: null
            }
          }
        ]
      },
      {
        path: 'about',
        component: AboutComponent,
        data: {
          navbarTitle: 'About',
          navbarRightButton: null
        }
      },
      {
        path: 'help',
        children: [
          {
            path: '',
            loadChildren: './help/help.module#HelpModule',
            data: {
              navbarTitle: 'Help',
              navbarLeftButton: null
            }
          },
          // {
          //   path: 'installation',
          //   component: HelpInstallationComponent,
          //   data: {
          //     navbarTitle: 'Help',
          //     navbarLeftButton: {
          //       back: true
          //     },
          //     navbarClass: 'bg-blue-light'
          //   }
          // },
          // {
          //   path: 'operation-guides',
          //   component: HelpOperationGuidesComponent,
          //   data: {
          //     navbarTitle: 'Help',
          //     navbarLeftButton: {
          //       back: true
          //     },
          //     navbarClass: 'bg-blue-light'
          //   }
          // },
          {
            path: 'faq',
            component: FAQComponent,
            data: {
              navbarTitle: 'FAQs',
              navbarLeftButton: {
                back: true
              }
            }
          },
          {
            path: 'contact-us',
            component: ContactUsComponent,
            data: {
              navbarTitle: 'Contact Us',
              navbarLeftButton: {
                back: true
              }
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
