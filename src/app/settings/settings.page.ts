import { Component, OnInit } from '@angular/core';

import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'stm-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss']
})
export class SettingsPageComponent implements OnInit {
  firstName: string = this.authService.userInfo.givenName;
  lastName: string = this.authService.userInfo.familyName;

  constructor(private authService: AuthService) {}

  ngOnInit() {}
}
