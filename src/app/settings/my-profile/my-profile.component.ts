import { Component, OnInit, Renderer } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../../shared/auth.service';

@Component({
  selector: 'stm-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {
  profile = {
    firstName: this.authService.userInfo.givenName,
    lastName: this.authService.userInfo.familyName,
    phoneNumber: this.authService.userInfo.phone,
    email: this.authService.userInfo.email
  };

  constructor(private router: Router, private authService: AuthService) {}

  ngOnInit(): void {}

  formDisabled(): boolean {
    return false;
  }

  submitForm(): void {
    if (!this.formDisabled()) {
      this.router.navigate(['/', 'change-password']);
    }
  }
}
