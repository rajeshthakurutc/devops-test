import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'stm-system-logs',
  templateUrl: './system-logs.component.html',
  styleUrls: ['./system-logs.component.scss']
})
export class SystemLogsComponent implements OnInit {

  lastXDays = 30;
  generatingLogs = false;

  constructor(
  ) {
  }

  ngOnInit(): void {
  }

  generateReports(): void {
    this.generatingLogs = !this.generatingLogs;

    if (this.generatingLogs) {
      console.log('TODO hit the API here');
    }
  }

  pluralize(count: number, noun: string, showNumber: boolean = true): string {
    return ((showNumber) ? `${count} ` : '') + `${noun}${(count !== 1) ? 's' : ''}`;
  }

}
