import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'stm-notification-management',
  templateUrl: './notification-management.component.html',
  styleUrls: ['./notification-management.component.scss']
})
export class NotificationManagementComponent implements OnInit {

  zones = {
    cooler: false,
    dineLine: false,
    dineReach: false,
    freezer: false,
  };

  constructor(
  ) {
  }

  ngOnInit(): void {
  }

}
