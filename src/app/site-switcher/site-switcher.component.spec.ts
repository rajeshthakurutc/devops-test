import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { PickerController } from '@ionic/angular';

import { SiteSwitcherComponent } from './site-switcher.component';

describe('SiteSwitcherComponent', () => {
  let component: SiteSwitcherComponent;
  let fixture: ComponentFixture<SiteSwitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SiteSwitcherComponent],
      providers: [HttpClient, HttpHandler, PickerController],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
