import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { PickerController } from '@ionic/angular';
import { PickerOptions } from '@ionic/core';

import { SiteSwitcherService } from './site-switcher.service';
import { ZonesService } from '../zones/zones.service';

@Component({
  selector: 'stm-site-switcher',
  templateUrl: './site-switcher.component.html',
  styleUrls: ['./site-switcher.component.scss']
})
export class SiteSwitcherComponent implements OnInit, OnDestroy {
  sites;
  defaultSite: string;
  showDropDownIcon = false;
  sitePicker;
  site = this.siteSwitcherService.getSavedSite();
  pickerConfig: PickerOptions;
  subscribe;
  fetchZoneSubscribe;

  constructor(
    private siteSwitcherService: SiteSwitcherService,
    private zonesService: ZonesService,
    private pickerCtrl: PickerController,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.subscribe = this.siteSwitcherService.fetchSites().subscribe(sites => {
      this.sites = sites;
      const siteExists = this.site || sites[0];
      // in rare case user doesn't have a site, prevent fetch of zones
      if (!!siteExists) {
        this.siteSwitcherService.setSiteId(this.site.siteId || sites[0].siteId);
        // wait for sites in order to fetch zones
        this.fetchZonesBySiteId(this.siteSwitcherService.siteId);
        this.defaultSite = this.site.customSiteId || sites[0].customSiteId;
      }

      this.showDropDownIcon = this.showIcon();
    });
  }

  ngOnDestroy() {
    this.subscribe.unsubscribe();

    // in case user leaves dashboard before selecting a new site check if subscribe exists
    if (this.fetchZoneSubscribe) {
      this.fetchZoneSubscribe.unsubscribe();
    }

    return this.sitePicker ? this.sitePicker.dismiss() : false;
  }

  showIcon(): boolean {
    return this.sites.length > 1 ? true : false;
  }

  saveSiteSelection(site) {
    this.siteSwitcherService.setSiteSelection(site);
  }

  fetchZonesBySiteId(siteId) {
    this.fetchZoneSubscribe = this.zonesService.fetchZonesBySiteId(siteId).subscribe(zones => {
      this.zonesService.updateZonesObservable(zones);
    });
  }

  async showSitePicker() {
    this.siteSwitcherService.createSitePickerOptions(this.sites);
    const options = this.siteSwitcherService.options;
    const selectedIndex = this.sites.findIndex(site => {
      return site.siteId === this.siteSwitcherService.siteId;
    });

    const pickerOptions: PickerOptions = {
      mode: 'ios',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.sitePicker.dismiss();
          }
        },
        {
          text: 'Done',
          handler: () => {
            this.sitePicker.onDidDismiss().then(async () => {
              const col = await this.sitePicker.getColumn('sites');
              const selection = col.options[col.selectedIndex];
              const siteId = selection.value;
              this.defaultSite = selection.text;
              this.cd.detectChanges();
              this.fetchZonesBySiteId(siteId);
              this.saveSiteSelection(selection);
            });
          }
        }
      ],
      columns: [
        {
          name: 'sites',
          selectedIndex,
          options
        }
      ]
    };

    this.sitePicker = await this.pickerCtrl.create(pickerOptions);
    this.sitePicker.present();
    this.cd.detectChanges();
  }
}
