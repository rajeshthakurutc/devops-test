import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
/* tslint:disable-next-line:import-blacklist */
import { Observable } from 'rxjs';

import { UtilsService } from '../shared/utils.service';
import { httpOptions, endpoints } from '../configs';

export interface ISite {
  customSiteId: string;
  locationId: number;
  name: string;
  programId: number;
  siteGroupIds: null;
  siteId: number;
  size: number;
  type: string;
}

@Injectable({
  providedIn: 'root'
})
export class SiteSwitcherService {
  public options = [];
  public siteId;
  constructor(private http: HttpClient, private utilsService: UtilsService) {}

  fetchSites(): Observable<ISite[]> {
    return this.http.get<ISite[]>(endpoints.getSites, httpOptions).pipe(
      tap(_ => this.utilsService.log('fetched sites')),
      map(sites => {
        // sort sites alphabetically
        sites.sort(this.siteSorter());
        return sites;
      }),
      catchError(this.utilsService.handleError('fetchSites', []))
    );
  }

  async createSitePickerOptions(sites) {
    const opts = sites.map(site => {
      return { text: site.customSiteId, value: site.siteId, ...site };
    });

    this.options = opts;
  }

  setSiteId(siteId): void {
    this.siteId = siteId;
  }

  getSiteId(): number {
    return this.siteId;
  }

  setSiteSelection(site: ISite): void {
    this.setSiteId(site.siteId);
    const siteObj = JSON.stringify(site);
    localStorage.setItem('selectedSite', siteObj);
  }

  getSavedSite(): ISite {
    const siteObject = localStorage.getItem('selectedSite');
    const site = !!siteObject ? JSON.parse(siteObject) : false;
    return site;
  }

  clearSavedSite(): void {
    localStorage.removeItem('selectedSite');
    this.siteId = null;
  }

  siteSorter() {
    return (site1, site2) => {
      const customSiteId1 = site1.customSiteId.trim().toLowerCase();
      const customSiteId2 = site2.customSiteId.trim().toLowerCase();
      if (customSiteId1 < customSiteId2) {
        return -1;
      }
      if (customSiteId1 > customSiteId2) {
        return 1;
      }
      // if two sites share the same customSiteId, be deterministic about display order
      const siteId1 = site1.siteId;
      const siteId2 = site2.siteId;
      if (siteId1 < siteId2) {
        return -1;
      }
      if (siteId1 > siteId2) {
        return 1;
      }
      return 0;
    };
  }
}
