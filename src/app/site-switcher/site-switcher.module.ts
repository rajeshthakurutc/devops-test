import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SiteSwitcherComponent } from './site-switcher.component';

@NgModule({
  imports: [CommonModule],
  declarations: [SiteSwitcherComponent],
  exports: [SiteSwitcherComponent]
})
export class SiteSwitcherModule {}
