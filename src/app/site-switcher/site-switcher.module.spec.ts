import { SiteSwitcherModule } from './site-switcher.module';

describe('SiteSwitcherModule', () => {
  let siteSwitcherModule: SiteSwitcherModule;

  beforeEach(() => {
    siteSwitcherModule = new SiteSwitcherModule();
  });

  it('should create an instance', () => {
    expect(siteSwitcherModule).toBeTruthy();
  });
});
