import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { SiteSwitcherService } from './site-switcher.service';

describe('SiteSwitcherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SiteSwitcherService]
    });
  });

  it('should be created', inject([SiteSwitcherService], (service: SiteSwitcherService) => {
    expect(service).toBeTruthy();
  }));

  it('should sort site customSiteIds correctly', () => {
    const service: SiteSwitcherService = TestBed.get(SiteSwitcherService);
    const sites = [
      {
        siteId: 2,
        customSiteId: ' z am i waiting'
      },
      {
        siteId: 3,
        customSiteId: 'My Freezer'
      },
      {
        siteId: 1,
        customSiteId: 'My Freezer'
      }
    ];
    const sortedSites = sites.sort(service.siteSorter());
    expect(sortedSites[0]['customSiteId']).toBe('My Freezer', 'm comes before z irrespective of leading whitespace');
    expect(sortedSites[0]['siteId']).toBe(1, 'items with same customSiteId to be sorted by siteId');
    expect(sortedSites[1]['customSiteId']).toBe('My Freezer', 'm comes before z irrespective of leading whitespace');
    expect(sortedSites[1]['siteId']).toBe(3, 'items with same customSiteId to be sorted by siteId');
    expect(sortedSites[2]['customSiteId']).toBe(
      ' z am i waiting',
      'z comes after m irrespective of leading whitespace'
    );
  });
});
