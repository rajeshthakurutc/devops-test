import { Monitor } from 'app/monitor';

export class Zone {
  id: number;
  siteId: number;
  name: string;
  type: string;
  status: string;
  tempType: string;
  monitors: Monitor[];
  lastUpdated: number;
  tempIdeal: number;
  minWarningTemperature: number;
  maxWarningTemperature: number;
  minCriticalTemperature: number;
  maxCriticalTemperature: number;
  lowContinuousWarning: number;
  highContinuousWarning: number;
  lowContinuousCritical: number;
  highContinuousCritical: number;
  normalOperations: number;
  history: any[];
  values: any[];

  constructor(args) {
    this.id = args.zoneId;
    this.siteId = args.siteId;
    this.name = args.name;
    this.monitors = args.monitorsNum;
    this.lastUpdated = args.modifiedOn;
    this.tempIdeal = args.tempIdeal;
    this.minWarningTemperature = args.minWarningTemperature;
    this.maxWarningTemperature = args.maxWarningTemperature;
    this.minCriticalTemperature = args.minCriticalTemperature;
    this.maxCriticalTemperature = args.maxCriticalTemperature;
    this.lowContinuousWarning = args.lowContinuousWarning;
    this.highContinuousWarning = args.highContinuousWarning;
    this.lowContinuousCritical = args.lowContinuousCritical;
    this.highContinuousCritical = args.highContinuousCritical;
    this.normalOperations = args.normalOperations;
    this.history = args.history;
    this.values = args.values;
  }

  addMonitor(monitor, index) {
    if (!monitor && isNaN(index)) {
      console.error('no monitor and/or index specified');
      return;
    }
    this.monitors[index] = monitor;
  }

  addMonitors(monitors) {
    if (Array.isArray(monitors)) {
      this.monitors.push(...monitors);
    } else {
      this.monitors.push(monitors);
    }
  }
}
