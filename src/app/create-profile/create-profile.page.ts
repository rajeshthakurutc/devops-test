import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'stm-create-profile',
  templateUrl: './create-profile.page.html',
  styleUrls: ['./create-profile.page.scss'],
})
export class CreateProfilePageComponent implements OnInit {

  firstName = '';
  lastName = '';
  jobTitle = '';
  phone = '';
  submitted = false;

  constructor(
    private router: Router,
    private location: Location
) { }

  ngOnInit() {
  }

  createProfile() {
    // Check for form validity as preCheck
    this.router.navigateByUrl('/installation-wizard');
  }

  onSubmit() {}

  back() {
    this.location.back();
  }

}
