import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { VisibilityInputComponent } from '../visibility-input/visibility-input.component';

import { CreateProfilePageComponent } from './create-profile.page';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('CreateProfilePage', () => {
  let component: CreateProfilePageComponent;
  let fixture: ComponentFixture<CreateProfilePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreateProfilePageComponent,
        VisibilityInputComponent,
      ],
      imports: [
        RouterTestingModule.withRoutes([]),
        FormsModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
