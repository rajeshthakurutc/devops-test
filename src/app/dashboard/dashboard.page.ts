import { Component, OnInit } from '@angular/core';

import { DataManagerService } from 'app/data-manager.service';
import { ZonesService } from '../zones/zones.service';
import { AnalyticsService } from '../shared/analytics.service';

import { Gateway } from 'app/gateway';
import { Zone } from 'app/zone';
import { Monitor } from 'app/monitor';
import { Task } from 'app/task';

@Component({
  selector: 'stm-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss']
})
export class DashboardPageComponent implements OnInit {
  gateways: Gateway[];
  zones: Zone[];
  monitors: Monitor[];
  tasks: Task[];
  showOpenTasks = true;

  constructor(
    public dataManagerService: DataManagerService,
    public zonesService: ZonesService,
    public analytics: AnalyticsService
  ) {}

  ngOnInit() {
    // // this.dataManagerService.gateways$.subscribe(data => (this.gateways = data));
    // // this.dataManagerService.monitors$.subscribe(data => (this.monitors = data));
    // this.dataManagerService.tasks$.subscribe(data => (this.tasks = data));
  }

  // getActiveTasks() {
  //   return this.tasks.filter(item => !item.resolved && !item.completed);
  // }

  // getInactiveTasks() {
  //   return this.tasks.filter(item => item.resolved && !item.completed);
  // }

  // hideOpenTasks() {
  //   this.showOpenTasks = false;
  // }
}
