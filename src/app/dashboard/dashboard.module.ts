import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MomentModule } from 'ngx-moment';

import { DashboardPageComponent } from './dashboard.page';
import { SiteSwitcherModule } from '../site-switcher/site-switcher.module';
import { ZonesModule } from '../zones/zones.module';

const routes: Routes = [
  {
    path: '',
    component: DashboardPageComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MomentModule,
    RouterModule.forChild(routes),
    SiteSwitcherModule,
    ZonesModule
  ],
  declarations: [DashboardPageComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardPageModule {}
