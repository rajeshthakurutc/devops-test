import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, async } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { Platform, IonicModule } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import { DataManagerService } from 'app/data-manager.service';
import { NavbarService } from './navbar/navbar.service';
import { ZonesService } from './zones/zones.service';
import { AuthService } from './shared/auth.service';
import { LoginService } from './login/login.service';
import { TermsService } from './terms/terms.service';
import { UtilsService } from './shared/utils.service';
import { AnalyticsService } from './shared/analytics.service';

describe('AppComponent', () => {
  let statusBarSpy, splashScreenSpy, platformReadySpy, platformSpy, dataManagerService, navbarSpy, routerSpy;

  beforeEach(async(() => {
    statusBarSpy = jasmine.createSpyObj('StatusBar', ['styleDefault']);
    splashScreenSpy = jasmine.createSpyObj('SplashScreen', ['hide']);
    platformReadySpy = Promise.resolve();
    navbarSpy = jasmine.createSpyObj('NavBar', ['']);
    routerSpy = jasmine.createSpyObj('Router', ['']);
    platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy });
    dataManagerService = new DataManagerService(new HttpClient(null));

    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [RouterTestingModule.withRoutes([]), IonicModule.forRoot(), HttpClientModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        NavbarService,
        DataManagerService,
        ZonesService,
        AuthService,
        LoginService,
        TermsService,
        UtilsService,
        AnalyticsService,
        { provide: StatusBar, useValue: statusBarSpy },
        { provide: SplashScreen, useValue: splashScreenSpy },
        { provide: Platform, useValue: platformSpy }
      ]
    }).compileComponents();
  }));

  it('should create the app', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
    expect(app).toBeTruthy();
  });

  it('should initialize the app', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    expect(platformSpy.ready).toHaveBeenCalled();
    await platformReadySpy;
    expect(statusBarSpy.styleDefault).toHaveBeenCalled();
    expect(splashScreenSpy.hide).toHaveBeenCalled();
  });

  /*
  it('should have pages defined', async () => {
    const app = new AppComponent(platformSpy, splashScreenSpy, statusBarSpy,
      dataManagerService, navbarSpy, routerSpy, routerSpy);
    expect(app.appPages.length).toBeGreaterThan(0);
  });
*/
  // Note: the test above causes other tests to break, throwing this in Chrome:
  //       "Uncaught TypeError: Cannot read property 'handle' of null thrown"

  // it('should have menu labels', async () => {
  //   const fixture = await TestBed.createComponent(AppComponent);
  //   await fixture.detectChanges();
  //   fixture.componentInstance.toggleMenu();
  //   fixture.detectChanges();
  //   const app = fixture.debugElement.nativeElement;
  //   console.log(app);
  //   const menuItems = app.querySelectorAll('li');
  //   expect(menuItems.length).toEqual(6);
  // });
});
