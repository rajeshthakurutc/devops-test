import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

import { NavbarService } from 'app/navbar/navbar.service';
import { environment } from '../environments/environment';
import { AuthService } from './shared/auth.service';
import { AnalyticsService } from './shared/analytics.service';

declare var cordova: any;
declare var Keyboard: any;

@Component({
  selector: 'stm-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  envTarget: string = environment.target;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    // temp only for showing tokens on app html
    private authService: AuthService,
    public navbar: NavbarService,
    protected router: Router,
    public analytics: AnalyticsService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // focused el might be out of viewport so scroll in into view
      window.addEventListener('native.keyboardshow', event => {
        document.activeElement.scrollIntoView();
      });

      // show "Done" button on native keyboard
      // Android
      if (Keyboard && Keyboard.hideFormAccessoryBar) {
        Keyboard.hideFormAccessoryBar(false);
      }
      // iOS
      if (Keyboard && Keyboard.hideKeyboardAccessoryBar) {
        Keyboard.hideKeyboardAccessoryBar(false);
      }
    });
  }

  clickedInside($event: Event) {
    $event.preventDefault();
    $event.stopPropagation();
  }
}
