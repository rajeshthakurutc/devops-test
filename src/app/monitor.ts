import { Zone } from 'app/zone';

export class Monitor {
  id: number;
  label: string;
  serialNumber: string;
  zoneId: number;
  state: number;
  values: any;
  lastUpdated: string;

  constructor(args) {
    this.id = args.deviceId;
    this.label = args.modelName;
    this.serialNumber = args.serialNo;
    this.zoneId = args.zoneId;
    this.state = args.state;
    this.lastUpdated = args.modifiedOn;
  }

  updateZone(zoneId) {
    if (!zoneId) {
      return;
    }
    this.zoneId = zoneId;
  }

  updateSerialNumber(newSerialNumber: string) {
    this.serialNumber = newSerialNumber;
  }

}
