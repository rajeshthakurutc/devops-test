import { Component, OnInit } from '@angular/core';
import Confetti from './confetti';

@Component({
  selector: 'stm-success-screen',
  templateUrl: './success-screen.component.html',
  styleUrls: ['./success-screen.component.scss']
})
export class SuccessScreenComponent implements OnInit {
  confetti;

  constructor() {}

  ngOnInit(): void {
    setTimeout(() => {
      // const containerEl = this.renderer.selectRootElement('stm-root');
      const containerEl = document.body;
      this.confetti = new Confetti(containerEl);
      this.confetti.init();
    }, 667);
  }
}
