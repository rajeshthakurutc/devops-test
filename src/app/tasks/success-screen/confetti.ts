// Original Swift 2.0 confetti code created by Jason Smith on 6/18/18.
// Copyright © 2018-2019 United Technologies. All rights reserved.

import { Random } from 'random-js';

const random = new Random();

type RGB = [number, number, number];

class Part {
  confetti = null;
  i = 0;
  isLive = true;

  x;
  y;
  r;
  d;
  color;
  tilt;
  tiltAngleIncrement;
  tiltAngle;

  vx = 0;
  vy = 0;
  rotDir = 1;

  constructor(confetti, i) {
    this.confetti = confetti;
    this.i = i;

    this.x = confetti.width / 2;
    this.y = 0;
    this.r = random.real(11, 33, true);
    this.tilt = random.real(11, 33, true);
    this.tiltAngleIncrement = random.real(0.07, 0.05, true);
    this.tiltAngle = 0;

    this.color = 'rgba(' + [...(<RGB>random.pick(confetti.palette)), random.real(0.5, 1, true)] + ')';

    if (random.bool()) {
      this.rotDir = -1;
    }

    this.vx = random.real(-0.7, 0.7, true) * 20.0;
    this.vy = random.real(-1.0, 0.0, true) * 55.0;

    this._move();
  }

  update(): boolean {
    if (this.isLive) {
      this._move();
    }

    return this.isLive;
  }

  _move(): void {
    /// add random movement to velocity
    this.vx += random.real(-1, 1, true) * 0.5;
    this.vy += random.real(-1, 1, true) * 0.2;

    /// add velocity to position
    this.x += this.vx;
    this.y += this.vy;

    /// apply gravity
    this.y += this.confetti.gravity;

    this.tiltAngle += this.tiltAngleIncrement;
    this.tilt = Math.sin(this.tiltAngle - this.i / 3) * 15 * this.rotDir;

    /// check boundaries
    this._wrap();
    this._killAtBottom();

    /// apply friction to velocity
    this.vx *= 0.95;
    this.vy *= 0.95;

    this.confetti.context.beginPath();
    this.confetti.context.lineWidth = this.r / 3;
    this.confetti.context.strokeStyle = this.color;
    this.confetti.context.moveTo(this.x + this.tilt + this.r / 2, this.y);
    this.confetti.context.lineTo(this.x + this.tilt, this.y + this.tilt + this.r / 5);
    this.confetti.context.stroke();
  }

  _wrap(): void {
    const right = this.confetti.width + 20;
    const left = -20;

    if (this.x < left) {
      this.x = right;
    }

    if (this.x > right) {
      this.x = left;
    }
  }

  _killAtBottom(): void {
    const bottom = this.confetti.height + 20;

    if (this.y > bottom) {
      this.isLive = false;
    }
  }
}

// tslint:disable-next-line: max-classes-per-file
class Confetti {
  targetEl: HTMLElement;
  numParts: number;
  gravity: number;
  allParts: Part[];

  _canvasEl: HTMLCanvasElement;
  context: CanvasRenderingContext2D;
  width: number;
  height: number;
  palette: RGB[] = [
    this._hexToRGB('#762eab'), // Purple
    this._hexToRGB('#e12c31'), // Red
    this._hexToRGB('#03ba7a'), // Green
    this._hexToRGB('#0e7de8'), // Blue
    this._hexToRGB('#efd775') // Yellow
  ];

  constructor(targetEl: HTMLElement, numParts: number = 50, gravity: number = 6) {
    if (targetEl instanceof HTMLElement) {
      this.targetEl = targetEl;
    } else {
      console.error('parameter targetEl must be instance of HTMLElement');
    }

    if (typeof numParts === 'number') {
      this.numParts = numParts;
    } else {
      console.warn('parameter numParts must be of number data type');
    }

    if (typeof gravity === 'number') {
      this.gravity = gravity;
    } else {
      console.warn('parameter gravity must be of number data type');
    }

    this.allParts = new Array(this.numParts);

    this._canvasEl = document.createElement('canvas');

    // Stretch the canvas and make it pass events thorough itself
    this._canvasEl.style.position = 'absolute';
    this._canvasEl.style.width = this._canvasEl.style.height = '100%';
    this._canvasEl.style.pointerEvents = 'none';

    this.context = this._canvasEl.getContext('2d');
  }

  init(): boolean {
    this.targetEl.appendChild(this._canvasEl);

    window.addEventListener('resize', this._handleResize);
    this._handleResize();

    for (let i = 0; i < this.numParts; i++) {
      this.allParts[i] = new Part(this, i);
    }

    this._startExplosion();

    return true;
  }

  _handleResize(): void {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this._canvasEl.width = window.innerWidth;
    this._canvasEl.height = window.innerHeight;
  }

  _startExplosion(): void {
    if (this.allParts.filter(p => !p.isLive).length === this.numParts) {
      this.destroy();
      return;
    }

    this.context.clearRect(0, 0, this.width, this.height);

    requestAnimationFrame(this._startExplosion.bind(this));

    for (let i = 0; i < this.numParts; i++) {
      this.allParts[i].update();
    }
  }

  destroy(): boolean {
    window.removeEventListener('resize', this._handleResize);

    if (this._canvasEl) {
      this._canvasEl.remove();
      delete this._canvasEl;
      return true;
    }

    return false;
  }

  _hexToRGB(hexColor: string): RGB {
    const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hexColor = hexColor.replace(shorthandRegex, (m, r, g, b) => {
      return r + r + g + g + b + b;
    });

    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexColor);

    return [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)];
  }
}

export default Confetti;
