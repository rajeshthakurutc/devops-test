import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'stm-corrective-action',
  templateUrl: './corrective-action.component.html',
  styleUrls: ['./corrective-action.component.scss']
})
export class CorrectiveActionComponent implements OnInit {
  options = [];
  selections = [];
  displayTextarea = false;
  otherResason = '';
  notes = '';
  id: number;

  constructor(private router: Router, private route: ActivatedRoute, private renderer: Renderer2) {
    this.options = [
      { key: 'doorClosed', value: 'Closed door' },
      { key: 'tempSettAdjstd', value: 'Adjusted temperature setting' },
      { key: 'monMovd', value: 'Moved monitor to another location' },
      {
        key: 'loremIps1',
        value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
      },
      {
        key: 'loremIps2',
        value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
      }
    ];
  }

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
  }

  onChange(name: string): void {
    const optionIsSelected = this.selections.includes(name);

    if (optionIsSelected) {
      this.selections = this.selections.filter(item => item !== name);
    } else {
      this.selections.push(name);
    }

    if (name === 'other') {
      this.displayTextarea = this.selections.includes('other');

      if (this.displayTextarea) {
        setTimeout(() => {
          const textArea = this.renderer.selectRootElement('#other_reason');
          textArea.focus();
        });
      }
    }
  }

  specifyOtherReason($event): void {
    this.otherResason = $event.target.value;
  }

  provideNotes($event): void {
    this.notes = $event.target.value;
  }

  formDisabled(): boolean {
    if (this.selections.length) {
      if (this.selections.includes('other')) {
        if (this.otherResason.trim() === '') {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    return true;
  }

  submitForm(): void {
    if (!this.formDisabled()) {
      this.router.navigate(['/', 'tasks', 'success-screen', this.id]);
    }
  }
}
