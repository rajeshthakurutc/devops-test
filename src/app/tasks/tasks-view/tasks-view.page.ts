import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataManagerService } from 'app/data-manager.service';
import { Task } from 'app/task';
import * as moment from 'moment';
import { ShortTask } from 'app/shortTask';

@Component({
  selector: 'stm-tasks-view',
  templateUrl: './tasks-view.page.html',
  styleUrls: ['./tasks-view.page.scss'],
})
export class TasksViewPageComponent implements OnInit {

  tasks: Task[];
  activeTemp: ShortTask[] = [];
  inactiveTemp: ShortTask[] = [];
  activeComm: ShortTask[] = [];
  inactiveComm: ShortTask[] = [];

  constructor(private dataManagerService: DataManagerService, private router: Router) { }

  ngOnInit() {
    this.dataManagerService.tasks$.subscribe(data => {
      this.tasks = data;
      console.log(this.tasks);
      this.sortTasks();
    });
  }

  sortTasks() {
    let num;
// tslint:disable-next-line: forin
    for (num in this.tasks) {
      const task = this.tasks[num];
      const data = new ShortTask(task);
      if (task.activeFlag) {
        if (task.alertType === 'zone') {
          this.activeTemp.push(data);
        } else {
          this.activeComm.push(data);
        }
      } else {
        if (task.alertType === 'zone') {
          this.inactiveTemp.push(data);
        } else {
          this.inactiveComm.push(data);
        }
      }
    }
    this.sortTasksFurther();
  }

  sortTasksFurther() {
    this.activeComm.sort((a, b ) => {
        if (a.created > b.created) {
          return -1;
        } else if (a.created < b.created) {
          return 1;
        } else {
          return 0;
        }
    });
    this.activeTemp.sort((a, b) => {
      if (a.status > b.status) {
        return -1;
      } else if (a.status < b.status) {
        return 1;
      } else {
        if (a.created > b.created) {
          return 1;
        } else if (a.created < b.created) {
          return -1;
        } else {
          return 0;
        }
      }
    });
  }

  openTemp(id) {
    this.router.navigateByUrl('/tasks/excursion-detail', id);
  }

}
