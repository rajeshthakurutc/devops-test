import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MomentModule } from 'ngx-moment';

import { TasksViewPageComponent } from './tasks-view.page';

const routes: Routes = [
  {
    path: '',
    component: TasksViewPageComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MomentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TasksViewPageComponent]
})
export class TasksViewPageModule {}
