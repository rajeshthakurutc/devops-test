import { Component, OnInit } from '@angular/core';
import { DataManagerService } from 'app/data-manager.service';
import { ActivatedRoute } from '@angular/router';
import { Task } from 'app/task';
import { ShortTask } from 'app/shortTask';

@Component({
  selector: 'stm-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss'],
})
export class TaskDetailComponent implements OnInit {

  id: number;
  tasks: Task[];
  dataSets: any[];
  overallTask: Task;
  data: ShortTask;
  graphOpen = false;
  historyOpen = false;
  zoneMonitors;

  constructor(
    private dataManagerService: DataManagerService,
    private router: ActivatedRoute
  ) {
    this.id = +this.router.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.dataManagerService.tasks$.subscribe(data => {
      if (!data.length) {
        return;
      }
      this.tasks = data;
      let num;

      // tslint:disable-next-line: forin
      for (num in this.tasks) {
        const task = this.tasks[num];
        if (task.id === this.id) {
          this.overallTask = task;
          this.data = new ShortTask(task);
          break;
        }
      }
      this.zoneMonitors = this.overallTask.ref.monitors;
    });
    // Note: these are hardcoded for now, later they'll need to be wired up
    this.dataSets = [
      {
        name: 'Monitor 1',
        values: [ 0, 14.9, 10, 5, 3, 4, 3, 5, -1, -1, null ],
        legend: {
          colorName: 'orange',
          label: '14.9°F',
        },
      },
      {
        name: 'Monitor 2',
        values: [ null, 0, 5, 4, 5, 6, 9, -1, 4, 5, null ],
        legend: {
          colorName: 'green',
          label: '5°F',
        },
      },
      {
        name: 'Monitor 3',
        values: [],
        legend: {
          colorName: 'red',
          label: null,
        }
      }
    ];
  }

  toggleGraph($event) {
    const el = $event.target.nextElementSibling;
    if (!this.graphOpen) {
      const h = $event.target.nextElementSibling.children[0].getBoundingClientRect().height;
      el.style.height = `${h}px`;
      this.graphOpen = true;
    } else {
      el.style.height = 0;
      this.graphOpen = false;
    }
  }

  toggleHistory($event) {
    const el = $event.target.nextElementSibling;
    if (!this.historyOpen) {
      const h = $event.target.nextElementSibling.children[0].getBoundingClientRect().height;
      el.style.height = `${h}px`;
      this.historyOpen = true;
    } else {
      el.style.height = 0;
      this.historyOpen = false;
    }
  }

}
