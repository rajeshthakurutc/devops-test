import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

import { AuthService } from './shared/auth.service';

@Injectable()
export class GlobalHTTPInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {}

  logOut() {
    alert('Your session has expired, please log in again.');
    this.authService.logOut();
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.router.url === '/login') {
      return next.handle(req);
    } else {
      return next.handle(req).catch(res => {
        console.log('Refresh token from interceptor', res.status);
        if (res.status === 401) {
          return this.authService.validateRefreshToken(this.authService.refreshToken).mergeMap((data: any) => {
            if (data.access_token !== '') {
              this.authService.setTokens(data);
            } else {
              this.logOut();
              return Observable.throwError(res);
            }

            const newReq = req.clone({
              headers: req.headers.set('Authorization', 'Bearer ' + this.authService.getAuthToken())
            });

            return next.handle(newReq);
          });
          // refresh token and auth token are invalid
        } else if (res.status === 500 || res.status === 403) {
          alert(`An error has occured.\n\n${res.message}`);
        } else if (res.error.error === 'invalid_grant') {
          this.logOut();
        }
        return Observable.throwError(res);
      });
    }
  }
}
