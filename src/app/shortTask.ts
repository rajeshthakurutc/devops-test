
export class ShortTask {
  id: number;
  activeFlag: boolean;
  low: number;
  high: number;
  type: string;
  name: string;
  serialNo: string;
  created: string;
  ago: string;
  duration: string = null;
  completed: string;
  ended: string = null;
  status: number;
  current: number;
  trending: string;
  descLong: string;

  constructor(args) {
    this.id = args.id;
    this.activeFlag = args.activeFlag;
    this.type = args.type;
    this.created = args.created;
    this.descLong = args.descriptionLong;
    if (args.activeFlag) {
      this.ago = args.created;
      if (args.alertType === 'zone') {
        this.setTempData(args.ref);
      } else {
        this.setCommData(args.ref);
      }
    } else {
      this.duration = args.created;
      this.completed = args.lastUpdated;
      if (args.alertType === 'zone') {
        this.setTempData(args.ref);
      } else {
        this.setCommData(args.ref);
      }
    }
  }

  public setCommData(data) {
    let monitor;
    for (monitor in data.monitors) {
      if (data.monitors[monitor].state === 2) {
        this.serialNo = data.monitors[monitor].serialNo;
        this.name = data.monitors[monitor].label;
        this.status = data.monitors[monitor].state;
        break;
      }
    }
  }

  public setTempData(data) {
    this.name = data.label;
    this.low = parseFloat(data.ranges.temp.low.warning);
    this.high = parseFloat(data.ranges.temp.high.warning);
    this.status = data.operationStatus;
    this.current = parseFloat(data.values.temp);
    this.trending = data.trending;
  }

  public setDuration(dur) {
    this.duration = dur;
  }

  public setEnded(end) {
    this.ended = end;
  }
}
