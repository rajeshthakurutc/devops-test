import { Component, OnInit, Input } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'stm-concentric-circle',
  templateUrl: './concentric-circle.component.html',
  styleUrls: ['./concentric-circle.component.scss']
})
export class ConcentricCircleComponent implements OnInit {
  @Input() status = '';
  @Input() zoneDetail = '';
  @Input() speed = 4;
  @Input() circles = 3;

  duration = this.speed + 's';
  begins = [];
  array = new Array(this.circles);

  _getBegin(i) {
    if (i === 0) {
      return '0s';
    }

    const seconds = (this.speed / this.circles) * i;
    return seconds + 's;op.end+' + seconds + 's';
  }

  constructor() {
    // It's better to cache these values
    // instead of making Angular templating engine call the function every time
    for (let i = 0; i < this.circles; i++) {
      this.begins[i] = this._getBegin(i);
    }
  }

  ngOnInit() {}
}
