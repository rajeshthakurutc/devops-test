import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ConcentricCircleComponent } from './concentric-circle.component';

describe('ConcentricCircleComponent', () => {
  let component: ConcentricCircleComponent;
  let fixture: ComponentFixture<ConcentricCircleComponent>;
  const status = 'Critical';
  const zoneDetail = 'ZoneDetailStatus' || undefined;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConcentricCircleComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcentricCircleComponent);
    component = fixture.componentInstance;
    component.status = status;
    component.zoneDetail = zoneDetail;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
