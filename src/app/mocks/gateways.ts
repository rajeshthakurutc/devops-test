import { Gateway } from 'app/gateway';

export const GATEWAYS: Gateway[] = [
  new Gateway({id: 123, label: 'Gateway', serialNumber: 'A1234567890'})
];
