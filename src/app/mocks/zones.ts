import { Zone } from 'app/zone';

const zones = [
  new Zone({
    id: 1,
    label: 'Refrigerator',
    monitors: [],
    maxMonitors: 2,
    installationStatus: null,
    operationStatus: null
  }),
  new Zone({ id: 2, label: 'Cooler', monitors: [], maxMonitors: 3, installationStatus: null, operationStatus: null }),
  new Zone({
    id: 3,
    label: 'Dine Line',
    monitors: [],
    maxMonitors: 1,
    installationStatus: null,
    operationStatus: null
  }),
  new Zone({
    id: 4,
    label: 'Dine Reach',
    monitors: [],
    maxMonitors: 5,
    installationStatus: null,
    operationStatus: null
  })
];

export const ZONES: Zone[] = zones;
