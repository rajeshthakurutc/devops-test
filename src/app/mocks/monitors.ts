import { Monitor } from 'app/monitor';

export const MONITORS: Monitor[] = [
  new Monitor({ id: 1, label: 'Temperature Monitor', serialNumber: 'A1234567001', zone: null }),
  new Monitor({ id: 2, label: 'Temperature Monitor', serialNumber: 'A1234567002', zone: null }),
  new Monitor({ id: 3, label: 'Temperature Monitor', serialNumber: 'A1234567003', zone: null }),
  new Monitor({ id: 4, label: 'Temperature Monitor', serialNumber: 'A1234567004', zone: null }),
  new Monitor({ id: 5, label: 'Temperature Monitor', serialNumber: 'A1234567005', zone: null }),
  new Monitor({ id: 6, label: 'Temperature Monitor', serialNumber: 'A1234567006', zone: null }),
  new Monitor({ id: 7, label: 'Temperature Monitor', serialNumber: 'A1234567007', zone: null }),
  new Monitor({ id: 8, label: 'Temperature Monitor', serialNumber: 'A1234567008', zone: null }),
  new Monitor({ id: 9, label: 'Temperature Monitor', serialNumber: 'A1234567009', zone: null }),
  new Monitor({ id: 10, label: 'Temperature Monitor', serialNumber: 'A1234567010', zone: null }),
  new Monitor({ id: 11, label: 'Temperature Monitor', serialNumber: 'A1234567011', zone: null }),
  new Monitor({ id: 12, label: 'Temperature Monitor', serialNumber: 'A1234567012', zone: null }),
  new Monitor({ id: 13, label: 'Temperature Monitor', serialNumber: 'A1234567013', zone: null }),
  new Monitor({ id: 14, label: 'Temperature Monitor', serialNumber: 'A1234567014', zone: null })
];
