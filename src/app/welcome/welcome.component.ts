import { Component, OnInit } from '@angular/core';

import { environment } from '../../environments/environment';

@Component({
  selector: 'stm-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  appVersion: string = environment.appVersion;
  isProd: boolean = environment.production;
  envTarget: string = environment.target;

  constructor() {}

  ngOnInit() {}
}
