import { Injectable } from '@angular/core';
/* tslint:disable-next-line:import-blacklist */
import { BehaviorSubject } from 'rxjs';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  public isLoading = new BehaviorSubject(false);

  constructor() {}
}
