import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'app/loader/loader.service';

@Component({
  selector: 'stm-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
  constructor(public loaderService: LoaderService) {}

  ngOnInit(): void {}
}
