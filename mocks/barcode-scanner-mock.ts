import {
    BarcodeScanner,
    BarcodeScannerOptions,
    BarcodeScanResult
} from '@ionic-native/barcode-scanner/ngx';

export class BarcodeScannerMock extends BarcodeScanner {
  scan (_options?: BarcodeScannerOptions): Promise<BarcodeScanResult> {
    const theResult: BarcodeScanResult = {
      format: 'CODE_39',
      cancelled: false,
      text: 'KL7BR7NK70',
    };

    return new Promise((resolve, _reject) => {
      resolve(theResult);
    });
  }
}
