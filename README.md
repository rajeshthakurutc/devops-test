[![Build Status](http://jenkins.prod.predikto.io:8080/buildStatus/icon?job=Rajesh-test)](http://jenkins.prod.predikto.io:8080/job/Rajesh-test/)    [![Coverage](http://10.0.148.19:9000/api/badges/measure?key=test-ui&metric=coverage&template=FLAT)](http://10.0.148.19:9000/dashboard?id=test-ui) [![Quality Gate](http://10.0.148.19:9000/api/badges/gate?key=test-ui&blinking=true&template=FLAT)](http://10.0.148.19:9000/dashboard?id=test-ui)  [![Test Sucess](http://10.0.148.19:9000/api/badges/measure?key=test-ui&metric=test_success_density&linking=true&template=FLAT)](http://10.0.148.19:9000/dashboard?id=test-ui)  [![Slack Channel](https://img.shields.io/badge/slack-join_channel-brightgreen.svg?style=flat-square)](https://utdigital.slack.com/messages/CL0CTEK19)

# exampleui
Playground to test Angular Ionic Cordova

This is simply a place for us to test the designs and set up of Ionic 4, Angular 6 and Cordova.

# To Use

1. Make sure that your npm installation path is directed towards Artifactory. The steps to do so are found here: http://rotor.dx.utc.com/docs/configuring-npm

2. After cloning the repository `npm install`

3. `npm run start`
